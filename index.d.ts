import Rx from 'rxjs';

interface ChirpeyServerApi {

  /************************************************
                      BASIC
  ************************************************/

    getVersion(): () => String;

    init: (options: {
      serverHost: String;
      serverPort: String;
      firebaseApiKey: String;
      firebaseAuthDomain: String;
      firebaseDatabaseUrl: String;
      firebaseStorageBucket: String;
      graphqlPath?: String,
      customFetch?: GlobalFetch
    }) => void;

    setUserToken: (token: String) => void;

    getUserToken: () => String;

    getServerEvents: (params: {
      name: String,
      fromDatetime: DateTime,
      limit: Number,
      offset: Number
    }) => Promise<[{
      name: String
      createdAt: DateTime;
      data: String
    }]>;

    // Returns the full URL for a file given its path
    // If a falsey value is passed, it returns `null`
    getUrl: (filePath: String | null) => string | null;

    /************************************************
                      COMMON
    ************************************************/

    uploadImages: (params: {
      images: [{
        // Full path to image file
        uri: String;
        // MIME type of image file
        // Ref: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
        type?: String;
      }]
    }) => Promise<{
      imageIds: [UUID]
    }>;

    isRunning: () => Promise<Boolean>;

    triggerEveryMinuteTask: () => Promise<String>;

    addThrowErrorTask: () => Promise<Boolean>;

    addDoNothingTask: () => Promise<Boolean>;

    destroyDialogSession: () => Promise<Void>;

    createDialogSession: (params: {
      userId: String;
    }) => Promise<{
      dialogUserId: UUID;
      onMessage: Rx.Observable<{
        dialogId: UUID;
      }>,
      onDisconnect: Rx.Observable<Void>;
      onConnect: Rx.Observable<Void>;
    }>;

    sendMessageInDialog: (params: {
      dialogId: UUID;
      dialogUserId: UUID;
      text: String;
    }) => Promise<DialogMessage>;

    sendMessageInDialogViaServer: (params: {
      dialogId: UUID;
      userId: UUID;
      text: String;
    }) => Promise<DialogMessage>;

    getMessagesInDialog: (params: {
      dialogId: UUID;
      limit: Numnber;
      offset: Number;
    }) => Promise<[DialogMessage]>;

    /************************************************
                      USERS
    ************************************************/

    login: (params: {
      email: String;
      password: String;
    }) => Promise<{
      user: {
        id: UUID;
        email: String;
        type: String;
        firstName: String;
        lastName: String;
        isVerified: Boolean;
        profileImage?: {
          path: String;
        }
      };
      token: String;
    }>;

    loginWithFacebook: (params: {
      fbAccessToken: String;
    }) => Promise<{
      user: {
        id: UUID;
        email: String;
        firstName: String;
        lastName: String;
      };
      token: String;
    }>;

    currentUser: () => Promise<{
      id: UUID;
      email: String;
      type: String;
      firstName: String;
      lastName: String;
      isVerified: Boolean;
      profileImage?: {
        path: String;
      }
    }>;

    userExist: (params: {
      email: String;
    }) => Promise<{
      userExist: Boolean
    }>;

    userProfile: (params: {
      userId: UUID
    }) => Promise<{
      id: UUID;
      email: String;
      firstName: String;
      lastName: String;
      bio?: String;
      location?: String;
      hometown?: String;
      dateOfBirth?: DateTime;
      profileImage?: {
        id: UUID;
        path: String;
      }
    }>;

    createUser: (params: {
      email: String;
      password: String;
      firstName: String;
      lastName: String;
    }) => Promise<{
      id: UUID;
    }>;

    currentUserIsOnboarded: () => Promise<Boolean>;

    currentUserVerificationHasSkippedImages: () => Promise<Boolean>;

    currentUserVerificationImages: () => Promise<[{
      path: String;
    }]>;

    updateUserIsOnboarded: (params: {
      userId: UUID;
      isOnboarded: Boolean;
    }) => Promise<void>;

    updateUserVerificationImages: (params: {
      userId: UUID;
      hasSkippedImages?: Boolean;
      imageIds?: Boolean;
    }) => Promise<void>;

    updateUserProfile: (params: {
      userId: UUID;
      email?: String;
      firstName?: String;
      lastName?: String;
      dateOfBirth?: DateTime;
      bio?: String;
      profileImageId?: UUID;
    }) => Promise<void>;

    /************************************************
                      FLOCKS
    ************************************************/

    createFlock: (params: {
      ownerId: UUID;
      name: String;
      description: String;
      mapLocationLatitude: Latitude;
      mapLocationLongitude: Longitude;
      mapLocationName: String;
      mapLocationAddress: String;
      dateStart: DateTime;
      dateEnd: DateTime;
      interestTags: [String];
      imageIds: [UUID]
    }) => Promise<{
      id: UUID;
      name: String;
    }>;

    updateFlock: (params: {
      flockId: UUID;
      name?: String;
      newOwnerId?: UUID;
      description?: String;
      mapLocationLatitude?: Latitude;
      mapLocationLongitude?: Longitude;
      mapLocationName?: String;
      mapLocationAddress?: String;
      dateStart?: DateTime;
      dateEnd?: DateTime;
      interestTags?: [String]
      imageIds?: [UUID]
    }) => Promise<{
      id: UUID;
      name: String;
    }>;

    deleteFlock: (params: {
      flockId: UUID;
    }) => Promise<{
      id: UUID;
    }>;

    joinFlock: (params: {
      flockId: UUID;
      userId: UUID;
    }) => Promise<{
      id: UUID;
      name: String;
    }>;

    leaveFlock: (params: {
      flockId: UUID;
      userId: UUID;
    }) => Promise<{
      id: UUID;
      name: String;
    }>;

    banFlockee: (params: {
      flockId: UUID;
      userId: UUID;
    }) => Promise<{
      id: UUID;
      name: String;
    }>;

    unbanFlockee: (params: {
      flockId: UUID;
      userId: UUID;
    }) => Promise<{
      id: UUID;
      name: String;
    }>;

    flockInterestTags: () => Promise<[String]>;

    flocksByCurrentUser: () => Promise<[{
      id: UUID;
      name: String;
      noOfFlockees: Number;
      mapLocation: {
        address: String;
        locality?: String;
        country?: String;
      };
      dateStart: DateTime;
      dateEnd: DateTime;
      interestTags: [String];
      images: [{
        path: String;
      }];
      owner: {
        id: UUID;
        firstName: String;
        lastName: String;
      };
    }]>;

    flocksUpcoming: (params: {
      offset?: Number = 0;
      limit?: Number = 25;
      filterByCountries?: [String];
      filterByInterestTags?: [String];
      filterByDateStart?: Datetime;
      filterByDateEnd?: Datetime;
    }) => Promise<[{
      id: UUID;
      name: String;
      noOfFlockees: Number;
      mapLocation: {
        address: String;
        locality?: String;
        country?: String;
      };
      dateStart: DateTime;
      dateEnd: DateTime;
      interestTags: [String];
      images: [{
        path: String;
      }];
      owner: {
        id: UUID;
        firstName: String;
        lastName: String;
        profileImage: {
          id: UUID;
          path: String;
        }
      };
    }]>;

    flocksUpcomingByLocality: (params: {
      offset?: Number = 0;
      limit?: Number = 25;
      flocksOffset?: Number = 0;
      flocksLimit?: Number = 25;
    }) => Promise<[{
      country: String;
      locality: String;
      noOfFlockeesInFlocksUpcoming: Number;
      flocksUpcoming: [{
        id: UUID;
        name: String;
        noOfFlockees: Number;
        mapLocation: {
          address: String;
          locality?: String;
          country?: String;
        };
        dateStart: DateTime;
        dateEnd: DateTime;
        interestTags: [String];
        images: [{
          path: String;
        }];
        owner: {
          id: UUID;
          firstName: String;
          lastName: String;
        };
      }];
    }]>;

    flocksUpcomingByCountry: (params: {
      offset?: Number = 0;
      limit?: Number = 25;
      flocksOffset?: Number = 0;
      flocksLimit?: Number = 25;
    }) => Promise<[{
      country: String;
      noOfFlockeesInFlocksUpcoming: Number;
      flocksUpcoming: [{
        id: UUID;
        name: String;
        noOfFlockees: Number;
        mapLocation: {
          address: String;
          locality?: String;
          country?: String;
        };
        dateStart: DateTime;
        dateEnd: DateTime;
        interestTags: [String];
        images: [{
          path: String;
        }];
        owner: {
          id: UUID;
          firstName: String;
          lastName: String;
        };
      }];
    }]>;

    flockDetail: (params: {
      flockId: UUID;
    }) => Promise<{
      id: UUID;
      name: String;
      description: String;
      noOfFlockees: Number;
      mapLocation: {
        address: String;
        name: String;
        latitude: Latitude;
        longitude: Longitude;
        locality?: String;
        country?: String;
      };
      dateStart: DateTime;
      dateEnd: DateTime;
      interestTags: [String];
      images: [{
        path: String;
      }];
      owner: {
        id: UUID;
        firstName: String;
        lastName: String;
        profileImage?: {
          id: UUID;
          path: String;
        }
      };
      flockees: [{
        id: UUID;
        firstName: String;
        lastName: String;
        profileImage?: {
          id: UUID;
          path: String;
        }
      }];
      bannedFlockees: [{
        id: UUID;
        firstName: String;
        lastName: String;
        profileImage?: {
          id: UUID;
          path: String;
        }
      }]
    }>;

    flockDialog: (params: {
      flockId: UUID;
    }) => Promise<{
      dialogId: UUID;
    }>;

    UserType: UserType
  }

  /************************************************
                        TYPES
  ************************************************/

  type DateTime = String;
  type UUID = String;
  type Latitude = Number;
  type Longitude = Number;

  interface UserType { USER, ADMIN, SUPERADMIN };

  interface DialogMessage {
    id: UUID;
    text: String;
    sender: {
      id: UUID;
      firstName: String;
      lastName: String;
      profileImage?: {
        id: UUID;
        path: String;
      }
    }
    sentAt: DateTime;
  }

  declare const chirpeyServerApi: ChirpeyServerApi;
  export = chirpeyServerApi;
