'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _apolloClient = require('apollo-client');

var _fetchEverywhere = require('fetch-everywhere');

var _fetchEverywhere2 = _interopRequireDefault(_fetchEverywhere);

var _apolloCacheInmemory = require('apollo-cache-inmemory');

var _createHttpLink = require('./createHttpLink');

var _createHttpLink2 = _interopRequireDefault(_createHttpLink);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createApolloClient(_ref) {
  var getUserToken = _ref.getUserToken,
      serverHost = _ref.serverHost,
      serverPort = _ref.serverPort,
      graphqlPath = _ref.graphqlPath,
      _ref$customFetch = _ref.customFetch,
      customFetch = _ref$customFetch === undefined ? _fetchEverywhere2.default : _ref$customFetch;

  var link = (0, _createHttpLink2.default)({
    serverHost: serverHost,
    serverPort: serverPort,
    graphqlPath: graphqlPath,
    getUserToken: getUserToken,
    customFetch: customFetch
  });
  var cache = new _apolloCacheInmemory.InMemoryCache();
  var client = new _apolloClient.ApolloClient({ link: link, cache: cache });
  return client;
}

exports.default = createApolloClient;
//# sourceMappingURL=createApolloClient.js.map