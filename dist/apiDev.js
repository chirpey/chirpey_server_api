'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var isRunning = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    var _ref2, isRunning;

    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject));

          case 2:
            _ref2 = _context.sent;
            isRunning = _ref2.healthTest.isRunning;
            return _context.abrupt('return', isRunning);

          case 5:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function isRunning() {
    return _ref.apply(this, arguments);
  };
}();

var triggerEveryMinuteTask = function () {
  var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject2));

          case 2:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function triggerEveryMinuteTask() {
    return _ref3.apply(this, arguments);
  };
}();

var addThrowErrorTask = function () {
  var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject3));

          case 2:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));

  return function addThrowErrorTask() {
    return _ref4.apply(this, arguments);
  };
}();

var addDoNothingTask = function () {
  var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
    return _regenerator2.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject4));

          case 2:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, this);
  }));

  return function addDoNothingTask() {
    return _ref5.apply(this, arguments);
  };
}();

var getServerEvents = function () {
  var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(_ref6) {
    var name = _ref6.name,
        fromDatetime = _ref6.fromDatetime,
        offset = _ref6.offset,
        limit = _ref6.limit;

    var _ref8, serverEvents;

    return _regenerator2.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject5, (0, _helpers.fieldsToString)({
              name: name,
              fromDatetime: fromDatetime,
              offset: offset,
              limit: limit
            })));

          case 2:
            _ref8 = _context5.sent;
            serverEvents = _ref8.serverEvents;
            return _context5.abrupt('return', serverEvents);

          case 5:
          case 'end':
            return _context5.stop();
        }
      }
    }, _callee5, this);
  }));

  return function getServerEvents(_x) {
    return _ref7.apply(this, arguments);
  };
}();

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        healthTest {\n          isRunning\n        }\n      }\n    '], ['\n      mutation {\n        healthTest {\n          isRunning\n        }\n      }\n    ']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        triggerEveryMinuteTask\n      }\n    '], ['\n      mutation {\n        triggerEveryMinuteTask\n      }\n    ']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        addThrowErrorTask\n      }\n    '], ['\n      mutation {\n        addThrowErrorTask\n      }\n    ']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        addDoNothingTask\n      }\n    '], ['\n      mutation {\n        addDoNothingTask\n      }\n    ']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        serverEvents\n          ', '\n        {\n          name\n          data\n          createdAt\n        }\n      }\n    '], ['\n      query {\n        serverEvents\n          ', '\n        {\n          name\n          data\n          createdAt\n        }\n      }\n    ']);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

var _baseApi = require('./baseApi');

var _baseApi2 = _interopRequireDefault(_baseApi);

var _helpers = require('./helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  isRunning: isRunning,
  triggerEveryMinuteTask: triggerEveryMinuteTask,
  addThrowErrorTask: addThrowErrorTask,
  addDoNothingTask: addDoNothingTask,
  getServerEvents: getServerEvents
};
//# sourceMappingURL=apiDev.js.map