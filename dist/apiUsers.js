'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var createUser = function () {
  var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref) {
    var email = _ref.email,
        password = _ref.password,
        firstName = _ref.firstName,
        lastName = _ref.lastName;

    var _ref3, id;

    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject, (0, _helpers.toString)(email), (0, _helpers.toString)(password), (0, _helpers.toString)(firstName), (0, _helpers.toString)(lastName)));

          case 2:
            _ref3 = _context.sent;
            id = _ref3.createUser.id;
            return _context.abrupt('return', { id: id });

          case 5:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function createUser(_x) {
    return _ref2.apply(this, arguments);
  };
}();

var login = function () {
  var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(_ref4) {
    var email = _ref4.email,
        password = _ref4.password;

    var _ref6, _ref6$login, user, token;

    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject2, (0, _helpers.toString)(email), (0, _helpers.toString)(password)));

          case 2:
            _ref6 = _context2.sent;
            _ref6$login = _ref6.login;
            user = _ref6$login.user;
            token = _ref6$login.token;
            return _context2.abrupt('return', { user: user, token: token });

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function login(_x2) {
    return _ref5.apply(this, arguments);
  };
}();

var loginWithFacebook = function () {
  var _ref8 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(_ref7) {
    var fbAccessToken = _ref7.fbAccessToken;

    var _ref9, _ref9$loginWithFacebo, user, token;

    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject3, (0, _helpers.toString)(fbAccessToken)));

          case 2:
            _ref9 = _context3.sent;
            _ref9$loginWithFacebo = _ref9.loginWithFacebook;
            user = _ref9$loginWithFacebo.user;
            token = _ref9$loginWithFacebo.token;
            return _context3.abrupt('return', { user: user, token: token });

          case 7:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));

  return function loginWithFacebook(_x3) {
    return _ref8.apply(this, arguments);
  };
}();

var currentUser = function () {
  var _ref10 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
    var _ref11, user;

    return _regenerator2.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject4));

          case 2:
            _ref11 = _context4.sent;
            user = _ref11.currentUser;
            return _context4.abrupt('return', user);

          case 5:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, this);
  }));

  return function currentUser() {
    return _ref10.apply(this, arguments);
  };
}();

var userExist = function () {
  var _ref13 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(_ref12) {
    var email = _ref12.email;

    var _ref14, userExist;

    return _regenerator2.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject5, (0, _helpers.toString)(email)));

          case 2:
            _ref14 = _context5.sent;
            userExist = _ref14.userExist;
            return _context5.abrupt('return', { userExist: userExist });

          case 5:
          case 'end':
            return _context5.stop();
        }
      }
    }, _callee5, this);
  }));

  return function userExist(_x4) {
    return _ref13.apply(this, arguments);
  };
}();

var userProfile = function () {
  var _ref16 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(_ref15) {
    var userId = _ref15.userId;

    var _ref17, _ref17$users, user;

    return _regenerator2.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject6, (0, _helpers.toString)(userId)));

          case 2:
            _ref17 = _context6.sent;
            _ref17$users = (0, _slicedToArray3.default)(_ref17.users, 1);
            user = _ref17$users[0];
            return _context6.abrupt('return', user);

          case 6:
          case 'end':
            return _context6.stop();
        }
      }
    }, _callee6, this);
  }));

  return function userProfile(_x5) {
    return _ref16.apply(this, arguments);
  };
}();

var currentUserIsOnboarded = function () {
  var _ref18 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7() {
    var _ref19, isOnboarded;

    return _regenerator2.default.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject7));

          case 2:
            _ref19 = _context7.sent;
            isOnboarded = _ref19.currentUser.isOnboarded;
            return _context7.abrupt('return', isOnboarded);

          case 5:
          case 'end':
            return _context7.stop();
        }
      }
    }, _callee7, this);
  }));

  return function currentUserIsOnboarded() {
    return _ref18.apply(this, arguments);
  };
}();

var currentUserVerificationHasSkippedImages = function () {
  var _ref20 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8() {
    var _ref21, hasSkippedImages;

    return _regenerator2.default.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject8));

          case 2:
            _ref21 = _context8.sent;
            hasSkippedImages = _ref21.currentUser.verification.hasSkippedImages;
            return _context8.abrupt('return', hasSkippedImages);

          case 5:
          case 'end':
            return _context8.stop();
        }
      }
    }, _callee8, this);
  }));

  return function currentUserVerificationHasSkippedImages() {
    return _ref20.apply(this, arguments);
  };
}();

var currentUserVerificationImages = function () {
  var _ref22 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9() {
    var _ref23, images;

    return _regenerator2.default.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject9));

          case 2:
            _ref23 = _context9.sent;
            images = _ref23.currentUser.verification.images;
            return _context9.abrupt('return', images);

          case 5:
          case 'end':
            return _context9.stop();
        }
      }
    }, _callee9, this);
  }));

  return function currentUserVerificationImages() {
    return _ref22.apply(this, arguments);
  };
}();

var updateUserIsOnboarded = function () {
  var _ref25 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee10(_ref24) {
    var userId = _ref24.userId,
        isOnboarded = _ref24.isOnboarded;
    return _regenerator2.default.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject10, (0, _helpers.toString)(userId), (0, _helpers.toString)(isOnboarded)));

          case 2:
          case 'end':
            return _context10.stop();
        }
      }
    }, _callee10, this);
  }));

  return function updateUserIsOnboarded(_x6) {
    return _ref25.apply(this, arguments);
  };
}();

var updateUserVerificationImages = function () {
  var _ref27 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee11(_ref26) {
    var userId = _ref26.userId,
        hasSkippedImages = _ref26.hasSkippedImages,
        imageIds = _ref26.imageIds;
    return _regenerator2.default.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject11, (0, _helpers.fieldsToString)({
              userId: userId,
              hasSkippedImages: hasSkippedImages,
              imageIds: imageIds
            })));

          case 2:
          case 'end':
            return _context11.stop();
        }
      }
    }, _callee11, this);
  }));

  return function updateUserVerificationImages(_x7) {
    return _ref27.apply(this, arguments);
  };
}();

var updateUserProfile = function () {
  var _ref29 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee12(_ref28) {
    var userId = _ref28.userId,
        email = _ref28.email,
        firstName = _ref28.firstName,
        lastName = _ref28.lastName,
        dateOfBirth = _ref28.dateOfBirth,
        bio = _ref28.bio,
        profileImageId = _ref28.profileImageId;
    return _regenerator2.default.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject12, (0, _helpers.fieldsToString)({
              userId: userId,
              email: email,
              firstName: firstName,
              lastName: lastName,
              dateOfBirth: dateOfBirth,
              bio: bio,
              profileImageId: profileImageId
            })));

          case 2:
          case 'end':
            return _context12.stop();
        }
      }
    }, _callee12, this);
  }));

  return function updateUserProfile(_x8) {
    return _ref29.apply(this, arguments);
  };
}();

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        createUser(\n          email: ', '\n          password: ', '\n          firstName: ', '\n          lastName: ', '\n        ){\n          id\n        }\n      }\n    '], ['\n      mutation {\n        createUser(\n          email: ', '\n          password: ', '\n          firstName: ', '\n          lastName: ', '\n        ){\n          id\n        }\n      }\n    ']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        login(\n          email: ', '\n          password: ', '\n        ){\n          token\n          user {\n            id\n            email\n            type\n            firstName\n            lastName\n            isVerified\n            profileImage {\n              id\n              path\n            }\n          }\n        }\n      }\n    '], ['\n      mutation {\n        login(\n          email: ', '\n          password: ', '\n        ){\n          token\n          user {\n            id\n            email\n            type\n            firstName\n            lastName\n            isVerified\n            profileImage {\n              id\n              path\n            }\n          }\n        }\n      }\n    ']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        loginWithFacebook(\n          fbAccessToken: ', '\n        ){\n          token\n          user {\n            id\n            email\n            firstName\n            lastName\n          }\n        }\n      }\n    '], ['\n      mutation {\n        loginWithFacebook(\n          fbAccessToken: ', '\n        ){\n          token\n          user {\n            id\n            email\n            firstName\n            lastName\n          }\n        }\n      }\n    ']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        currentUser {\n          id\n          email\n          type\n          firstName\n          lastName\n          isVerified\n          profileImage {\n            id\n            path\n          }\n        }\n      }\n    '], ['\n      query {\n        currentUser {\n          id\n          email\n          type\n          firstName\n          lastName\n          isVerified\n          profileImage {\n            id\n            path\n          }\n        }\n      }\n    ']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        userExist(email: ', ')\n      }\n    '], ['\n      query {\n        userExist(email: ', ')\n      }\n    ']),
    _templateObject6 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        users(ids: [', ']) {\n          id\n          email\n          firstName\n          lastName\n          bio\n          location\n          hometown\n          dateOfBirth\n          profileImage {\n            id\n            path\n          }\n        }\n      }\n    '], ['\n      query {\n        users(ids: [', ']) {\n          id\n          email\n          firstName\n          lastName\n          bio\n          location\n          hometown\n          dateOfBirth\n          profileImage {\n            id\n            path\n          }\n        }\n      }\n    ']),
    _templateObject7 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        currentUser {\n          id\n          isOnboarded\n        }\n      }\n    '], ['\n      query {\n        currentUser {\n          id\n          isOnboarded\n        }\n      }\n    ']),
    _templateObject8 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        currentUser {\n          id\n          verification {\n            hasSkippedImages\n          }\n        }\n      }\n    '], ['\n      query {\n        currentUser {\n          id\n          verification {\n            hasSkippedImages\n          }\n        }\n      }\n    ']),
    _templateObject9 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        currentUser {\n          id\n          verification {\n            images {\n              id\n              path\n            }\n          }\n        }\n      }\n    '], ['\n      query {\n        currentUser {\n          id\n          verification {\n            images {\n              id\n              path\n            }\n          }\n        }\n      }\n    ']),
    _templateObject10 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        updateUser(\n          userId: ', '\n          isOnboarded: ', '\n        ) {\n          id\n          isOnboarded\n        }\n      }\n    '], ['\n      mutation {\n        updateUser(\n          userId: ', '\n          isOnboarded: ', '\n        ) {\n          id\n          isOnboarded\n        }\n      }\n    ']),
    _templateObject11 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        updateUserVerificationImages\n          ', '\n        {\n          id\n          verification {\n            hasSkippedImages\n            images {\n              id\n              path\n            }\n          }\n        }\n      }\n    '], ['\n      mutation {\n        updateUserVerificationImages\n          ', '\n        {\n          id\n          verification {\n            hasSkippedImages\n            images {\n              id\n              path\n            }\n          }\n        }\n      }\n    ']),
    _templateObject12 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        updateUser\n          ', '\n        {\n          id\n          email\n          firstName\n          lastName\n          dateOfBirth\n          profileImage {\n            id\n            path\n          }\n          bio\n        }\n      }\n    '], ['\n      mutation {\n        updateUser\n          ', '\n        {\n          id\n          email\n          firstName\n          lastName\n          dateOfBirth\n          profileImage {\n            id\n            path\n          }\n          bio\n        }\n      }\n    ']);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _helpers = require('./helpers');

var _baseApi = require('./baseApi');

var _baseApi2 = _interopRequireDefault(_baseApi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserType = {
  USER: 'USER',
  ADMIN: 'ADMIN',
  SUPERADMIN: 'SUPERADMIN'
};

exports.default = {
  UserType: UserType,
  login: login,
  loginWithFacebook: loginWithFacebook,
  currentUser: currentUser,
  createUser: createUser,
  userProfile: userProfile,
  userExist: userExist,
  currentUserIsOnboarded: currentUserIsOnboarded,
  currentUserVerificationHasSkippedImages: currentUserVerificationHasSkippedImages,
  currentUserVerificationImages: currentUserVerificationImages,
  updateUserProfile: updateUserProfile,
  updateUserVerificationImages: updateUserVerificationImages,
  updateUserIsOnboarded: updateUserIsOnboarded
};
//# sourceMappingURL=apiUsers.js.map