'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var createFlock = function () {
  var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref) {
    var ownerId = _ref.ownerId,
        name = _ref.name,
        description = _ref.description,
        mapLocationLatitude = _ref.mapLocationLatitude,
        mapLocationLongitude = _ref.mapLocationLongitude,
        mapLocationName = _ref.mapLocationName,
        mapLocationAddress = _ref.mapLocationAddress,
        dateStart = _ref.dateStart,
        dateEnd = _ref.dateEnd,
        interestTags = _ref.interestTags,
        imageIds = _ref.imageIds;

    var _ref3, _ref3$createFlock, id, newName;

    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject, (0, _helpers.fieldsToString)({
              ownerId: ownerId,
              name: name,
              description: description,
              mapLocationLatitude: mapLocationLatitude,
              mapLocationLongitude: mapLocationLongitude,
              mapLocationName: mapLocationName,
              mapLocationAddress: mapLocationAddress,
              dateStart: dateStart,
              dateEnd: dateEnd,
              interestTags: interestTags,
              imageIds: imageIds
            })));

          case 2:
            _ref3 = _context.sent;
            _ref3$createFlock = _ref3.createFlock;
            id = _ref3$createFlock.id;
            newName = _ref3$createFlock.name;
            return _context.abrupt('return', { id: id, name: newName });

          case 7:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function createFlock(_x) {
    return _ref2.apply(this, arguments);
  };
}();

var updateFlock = function () {
  var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(_ref4) {
    var flockId = _ref4.flockId,
        currentOwnerId = _ref4.currentOwnerId,
        name = _ref4.name,
        newOwnerId = _ref4.newOwnerId,
        description = _ref4.description,
        mapLocationLatitude = _ref4.mapLocationLatitude,
        mapLocationLongitude = _ref4.mapLocationLongitude,
        mapLocationName = _ref4.mapLocationName,
        mapLocationAddress = _ref4.mapLocationAddress,
        dateStart = _ref4.dateStart,
        dateEnd = _ref4.dateEnd,
        interestTags = _ref4.interestTags,
        imageIds = _ref4.imageIds;

    var _ref6, _ref6$updateFlock, id, newName;

    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject2, (0, _helpers.fieldsToString)({
              flockId: flockId,
              currentOwnerId: currentOwnerId,
              name: name,
              newOwnerId: newOwnerId,
              description: description,
              mapLocationLatitude: mapLocationLatitude,
              mapLocationLongitude: mapLocationLongitude,
              mapLocationName: mapLocationName,
              mapLocationAddress: mapLocationAddress,
              dateStart: dateStart,
              dateEnd: dateEnd,
              interestTags: interestTags,
              imageIds: imageIds
            })));

          case 2:
            _ref6 = _context2.sent;
            _ref6$updateFlock = _ref6.updateFlock;
            id = _ref6$updateFlock.id;
            newName = _ref6$updateFlock.name;
            return _context2.abrupt('return', { id: id, name: newName });

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function updateFlock(_x2) {
    return _ref5.apply(this, arguments);
  };
}();

var deleteFlock = function () {
  var _ref8 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(_ref7) {
    var flockId = _ref7.flockId;

    var _ref9, id;

    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject3, (0, _helpers.toString)(flockId)));

          case 2:
            _ref9 = _context3.sent;
            id = _ref9.deleteFlock;
            return _context3.abrupt('return', { id: id });

          case 5:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));

  return function deleteFlock(_x3) {
    return _ref8.apply(this, arguments);
  };
}();

var joinFlock = function () {
  var _ref11 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(_ref10) {
    var flockId = _ref10.flockId,
        userId = _ref10.userId;

    var _ref12, _ref12$joinFlock, id, name;

    return _regenerator2.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject4, (0, _helpers.toString)(flockId), (0, _helpers.toString)(userId)));

          case 2:
            _ref12 = _context4.sent;
            _ref12$joinFlock = _ref12.joinFlock;
            id = _ref12$joinFlock.id;
            name = _ref12$joinFlock.name;
            return _context4.abrupt('return', { id: id, name: name });

          case 7:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, this);
  }));

  return function joinFlock(_x4) {
    return _ref11.apply(this, arguments);
  };
}();

var leaveFlock = function () {
  var _ref14 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(_ref13) {
    var flockId = _ref13.flockId,
        userId = _ref13.userId;

    var _ref15, _ref15$leaveFlock, id, name;

    return _regenerator2.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject5, (0, _helpers.toString)(flockId), (0, _helpers.toString)(userId)));

          case 2:
            _ref15 = _context5.sent;
            _ref15$leaveFlock = _ref15.leaveFlock;
            id = _ref15$leaveFlock.id;
            name = _ref15$leaveFlock.name;
            return _context5.abrupt('return', { id: id, name: name });

          case 7:
          case 'end':
            return _context5.stop();
        }
      }
    }, _callee5, this);
  }));

  return function leaveFlock(_x5) {
    return _ref14.apply(this, arguments);
  };
}();

var banFlockee = function () {
  var _ref17 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(_ref16) {
    var flockId = _ref16.flockId,
        userId = _ref16.userId;

    var _ref18, _ref18$banFlockee, id, name;

    return _regenerator2.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject6, (0, _helpers.toString)(flockId), (0, _helpers.toString)(userId)));

          case 2:
            _ref18 = _context6.sent;
            _ref18$banFlockee = _ref18.banFlockee;
            id = _ref18$banFlockee.id;
            name = _ref18$banFlockee.name;
            return _context6.abrupt('return', { id: id, name: name });

          case 7:
          case 'end':
            return _context6.stop();
        }
      }
    }, _callee6, this);
  }));

  return function banFlockee(_x6) {
    return _ref17.apply(this, arguments);
  };
}();

var unbanFlockee = function () {
  var _ref20 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7(_ref19) {
    var flockId = _ref19.flockId,
        userId = _ref19.userId;

    var _ref21, _ref21$unbanFlockee, id, name;

    return _regenerator2.default.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject7, (0, _helpers.toString)(flockId), (0, _helpers.toString)(userId)));

          case 2:
            _ref21 = _context7.sent;
            _ref21$unbanFlockee = _ref21.unbanFlockee;
            id = _ref21$unbanFlockee.id;
            name = _ref21$unbanFlockee.name;
            return _context7.abrupt('return', { id: id, name: name });

          case 7:
          case 'end':
            return _context7.stop();
        }
      }
    }, _callee7, this);
  }));

  return function unbanFlockee(_x7) {
    return _ref20.apply(this, arguments);
  };
}();

// TODO: Have dynamic tags


var flockInterestTags = function () {
  var _ref22 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8() {
    return _regenerator2.default.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            return _context8.abrupt('return', ['Food', 'Adventure', 'Relaxation', 'Sightseeing', 'Urban']);

          case 1:
          case 'end':
            return _context8.stop();
        }
      }
    }, _callee8, this);
  }));

  return function flockInterestTags() {
    return _ref22.apply(this, arguments);
  };
}();

var flocksByCurrentUser = function () {
  var _ref23 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9() {
    var _ref24, flocks;

    return _regenerator2.default.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject8));

          case 2:
            _ref24 = _context9.sent;
            flocks = _ref24.currentUser.flocks;
            return _context9.abrupt('return', flocks);

          case 5:
          case 'end':
            return _context9.stop();
        }
      }
    }, _callee9, this);
  }));

  return function flocksByCurrentUser() {
    return _ref23.apply(this, arguments);
  };
}();

var flocksUpcoming = function () {
  var _ref26 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee10(_ref25) {
    var _ref25$offset = _ref25.offset,
        offset = _ref25$offset === undefined ? 0 : _ref25$offset,
        _ref25$limit = _ref25.limit,
        limit = _ref25$limit === undefined ? 25 : _ref25$limit,
        filterByCountries = _ref25.filterByCountries,
        filterByInterestTags = _ref25.filterByInterestTags,
        filterByDateStart = _ref25.filterByDateStart,
        filterByDateEnd = _ref25.filterByDateEnd;

    var _ref27, flocks;

    return _regenerator2.default.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject9, (0, _helpers.fieldsToString)({
              offset: offset || 0,
              limit: limit || 25,
              filterByCountries: filterByCountries,
              filterByInterestTags: filterByInterestTags,
              filterByDateStart: filterByDateStart,
              filterByDateEnd: filterByDateEnd
            })));

          case 2:
            _ref27 = _context10.sent;
            flocks = _ref27.flocksUpcoming;
            return _context10.abrupt('return', flocks);

          case 5:
          case 'end':
            return _context10.stop();
        }
      }
    }, _callee10, this);
  }));

  return function flocksUpcoming(_x8) {
    return _ref26.apply(this, arguments);
  };
}();

var flocksUpcomingByLocality = function () {
  var _ref29 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee11(_ref28) {
    var _ref28$offset = _ref28.offset,
        offset = _ref28$offset === undefined ? 0 : _ref28$offset,
        _ref28$limit = _ref28.limit,
        limit = _ref28$limit === undefined ? 25 : _ref28$limit,
        _ref28$flocksOffset = _ref28.flocksOffset,
        flocksOffset = _ref28$flocksOffset === undefined ? 0 : _ref28$flocksOffset,
        _ref28$flocksLimit = _ref28.flocksLimit,
        flocksLimit = _ref28$flocksLimit === undefined ? 25 : _ref28$flocksLimit;

    var _ref30, flockLocalities;

    return _regenerator2.default.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject10, offset, limit, flocksOffset, flocksLimit));

          case 2:
            _ref30 = _context11.sent;
            flockLocalities = _ref30.flockLocalities;
            return _context11.abrupt('return', flockLocalities);

          case 5:
          case 'end':
            return _context11.stop();
        }
      }
    }, _callee11, this);
  }));

  return function flocksUpcomingByLocality(_x9) {
    return _ref29.apply(this, arguments);
  };
}();

var flocksUpcomingByCountry = function () {
  var _ref32 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee12(_ref31) {
    var _ref31$offset = _ref31.offset,
        offset = _ref31$offset === undefined ? 0 : _ref31$offset,
        _ref31$limit = _ref31.limit,
        limit = _ref31$limit === undefined ? 25 : _ref31$limit,
        _ref31$flocksOffset = _ref31.flocksOffset,
        flocksOffset = _ref31$flocksOffset === undefined ? 0 : _ref31$flocksOffset,
        _ref31$flocksLimit = _ref31.flocksLimit,
        flocksLimit = _ref31$flocksLimit === undefined ? 25 : _ref31$flocksLimit;

    var _ref33, flockCountries;

    return _regenerator2.default.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject11, offset, limit, flocksOffset, flocksLimit));

          case 2:
            _ref33 = _context12.sent;
            flockCountries = _ref33.flockCountries;
            return _context12.abrupt('return', flockCountries);

          case 5:
          case 'end':
            return _context12.stop();
        }
      }
    }, _callee12, this);
  }));

  return function flocksUpcomingByCountry(_x10) {
    return _ref32.apply(this, arguments);
  };
}();

var flockDetail = function () {
  var _ref35 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee13(_ref34) {
    var flockId = _ref34.flockId;

    var _ref36, _ref36$flocks, flock;

    return _regenerator2.default.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            _context13.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject12, (0, _helpers.toString)(flockId)));

          case 2:
            _ref36 = _context13.sent;
            _ref36$flocks = (0, _slicedToArray3.default)(_ref36.flocks, 1);
            flock = _ref36$flocks[0];
            return _context13.abrupt('return', flock);

          case 6:
          case 'end':
            return _context13.stop();
        }
      }
    }, _callee13, this);
  }));

  return function flockDetail(_x11) {
    return _ref35.apply(this, arguments);
  };
}();

var flockDialog = function () {
  var _ref38 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee14(_ref37) {
    var flockId = _ref37.flockId;

    var _ref39, _ref39$flocks, flock;

    return _regenerator2.default.wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            _context14.next = 2;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject13, (0, _helpers.toString)(flockId)));

          case 2:
            _ref39 = _context14.sent;
            _ref39$flocks = (0, _slicedToArray3.default)(_ref39.flocks, 1);
            flock = _ref39$flocks[0];
            return _context14.abrupt('return', flock);

          case 6:
          case 'end':
            return _context14.stop();
        }
      }
    }, _callee14, this);
  }));

  return function flockDialog(_x12) {
    return _ref38.apply(this, arguments);
  };
}();

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        createFlock\n          ', '\n        {\n          id\n          name\n        }\n      }\n    '], ['\n      mutation {\n        createFlock\n          ', '\n        {\n          id\n          name\n        }\n      }\n    ']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        updateFlock\n          ', '\n        {\n          id\n          name\n        }\n      }\n    '], ['\n      mutation {\n        updateFlock\n          ', '\n        {\n          id\n          name\n        }\n      }\n    ']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        deleteFlock(\n          flockId: ', '\n        )\n      }\n    '], ['\n      mutation {\n        deleteFlock(\n          flockId: ', '\n        )\n      }\n    ']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        joinFlock(\n          flockId: ', '\n          userId: ', '\n        ) {\n          id\n          name\n        }\n      }\n    '], ['\n      mutation {\n        joinFlock(\n          flockId: ', '\n          userId: ', '\n        ) {\n          id\n          name\n        }\n      }\n    ']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        leaveFlock(\n          flockId: ', '\n          userId: ', '\n        ) {\n          id\n          name\n        }\n      }\n    '], ['\n      mutation {\n        leaveFlock(\n          flockId: ', '\n          userId: ', '\n        ) {\n          id\n          name\n        }\n      }\n    ']),
    _templateObject6 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        banFlockee(\n          flockId: ', '\n          userId: ', '\n        ) {\n          id\n          name\n        }\n      }\n    '], ['\n      mutation {\n        banFlockee(\n          flockId: ', '\n          userId: ', '\n        ) {\n          id\n          name\n        }\n      }\n    ']),
    _templateObject7 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        unbanFlockee(\n          flockId: ', '\n          userId: ', '\n        ) {\n          id\n          name\n        }\n      }\n    '], ['\n      mutation {\n        unbanFlockee(\n          flockId: ', '\n          userId: ', '\n        ) {\n          id\n          name\n        }\n      }\n    ']),
    _templateObject8 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        currentUser {\n          id\n          flocks {\n            id\n            name\n            noOfFlockees\n            mapLocation {\n              locality\n              country\n              address\n            }\n            dateStart\n            dateEnd\n            interestTags\n            images {\n              id\n              path\n            }\n            owner {\n              id\n              firstName\n              lastName\n            }\n          }\n        }\n      }\n    '], ['\n      query {\n        currentUser {\n          id\n          flocks {\n            id\n            name\n            noOfFlockees\n            mapLocation {\n              locality\n              country\n              address\n            }\n            dateStart\n            dateEnd\n            interestTags\n            images {\n              id\n              path\n            }\n            owner {\n              id\n              firstName\n              lastName\n            }\n          }\n        }\n      }\n    ']),
    _templateObject9 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        flocksUpcoming\n          ', '\n        {\n          id\n          name\n          noOfFlockees\n          mapLocation {\n            locality\n            country\n            address\n          }\n          dateStart\n          dateEnd\n          interestTags\n          images {\n            id\n            path\n          }\n          owner {\n            id\n            firstName\n            lastName\n            profileImage {\n              id\n              path\n            }\n          }\n        }\n      }\n    '], ['\n      query {\n        flocksUpcoming\n          ', '\n        {\n          id\n          name\n          noOfFlockees\n          mapLocation {\n            locality\n            country\n            address\n          }\n          dateStart\n          dateEnd\n          interestTags\n          images {\n            id\n            path\n          }\n          owner {\n            id\n            firstName\n            lastName\n            profileImage {\n              id\n              path\n            }\n          }\n        }\n      }\n    ']),
    _templateObject10 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        flockLocalities(\n          offset: ', '\n          limit: ', '\n        ) {\n          country\n          locality\n          noOfFlockeesInFlocksUpcoming\n          flocksUpcoming(\n            offset: ', '\n            limit: ', '\n          ) {\n            id\n            name\n            noOfFlockees\n            mapLocation {\n              locality\n              country\n              address\n            }\n            dateStart\n            dateEnd\n            interestTags\n            images {\n              id\n              path\n            }\n            owner {\n              id\n              firstName\n              lastName\n            }\n          }\n        }\n      }\n    '], ['\n      query {\n        flockLocalities(\n          offset: ', '\n          limit: ', '\n        ) {\n          country\n          locality\n          noOfFlockeesInFlocksUpcoming\n          flocksUpcoming(\n            offset: ', '\n            limit: ', '\n          ) {\n            id\n            name\n            noOfFlockees\n            mapLocation {\n              locality\n              country\n              address\n            }\n            dateStart\n            dateEnd\n            interestTags\n            images {\n              id\n              path\n            }\n            owner {\n              id\n              firstName\n              lastName\n            }\n          }\n        }\n      }\n    ']),
    _templateObject11 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        flockCountries(\n          offset: ', '\n          limit: ', '\n        ) {\n          country\n          noOfFlockeesInFlocksUpcoming\n          flocksUpcoming(\n            offset: ', '\n            limit: ', '\n          ) {\n            id\n            name\n            noOfFlockees\n            mapLocation {\n              locality\n              country\n              address\n            }\n            dateStart\n            dateEnd\n            interestTags\n            images {\n              id\n              path\n            }\n            owner {\n              id\n              firstName\n              lastName\n            }\n          }\n        }\n      }\n    '], ['\n      query {\n        flockCountries(\n          offset: ', '\n          limit: ', '\n        ) {\n          country\n          noOfFlockeesInFlocksUpcoming\n          flocksUpcoming(\n            offset: ', '\n            limit: ', '\n          ) {\n            id\n            name\n            noOfFlockees\n            mapLocation {\n              locality\n              country\n              address\n            }\n            dateStart\n            dateEnd\n            interestTags\n            images {\n              id\n              path\n            }\n            owner {\n              id\n              firstName\n              lastName\n            }\n          }\n        }\n      }\n    ']),
    _templateObject12 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        flocks(\n          ids: [', ']\n        ) {\n          id\n          name\n          description\n          noOfFlockees\n          mapLocation {\n            name\n            country\n            locality\n            address\n            latitude\n            longitude\n          }\n          dateStart\n          dateEnd\n          interestTags\n          images {\n            id\n            path\n          }\n          owner {\n            id\n            firstName\n            lastName\n            profileImage {\n              id\n              path\n            }\n          }\n          flockees {\n            id\n            firstName\n            lastName\n            profileImage {\n              id\n              path\n            }\n          }\n          bannedFlockees {\n            id\n            firstName\n            lastName\n            profileImage {\n              id\n              path\n            }\n          }\n        }\n      }\n    '], ['\n      query {\n        flocks(\n          ids: [', ']\n        ) {\n          id\n          name\n          description\n          noOfFlockees\n          mapLocation {\n            name\n            country\n            locality\n            address\n            latitude\n            longitude\n          }\n          dateStart\n          dateEnd\n          interestTags\n          images {\n            id\n            path\n          }\n          owner {\n            id\n            firstName\n            lastName\n            profileImage {\n              id\n              path\n            }\n          }\n          flockees {\n            id\n            firstName\n            lastName\n            profileImage {\n              id\n              path\n            }\n          }\n          bannedFlockees {\n            id\n            firstName\n            lastName\n            profileImage {\n              id\n              path\n            }\n          }\n        }\n      }\n    ']),
    _templateObject13 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        flocks(\n          ids: [', ']\n        ) {\n          dialogId\n        }\n      }\n    '], ['\n      query {\n        flocks(\n          ids: [', ']\n        ) {\n          dialogId\n        }\n      }\n    ']);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

var _helpers = require('./helpers');

var _baseApi = require('./baseApi');

var _baseApi2 = _interopRequireDefault(_baseApi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  createFlock: createFlock,
  updateFlock: updateFlock,
  flockInterestTags: flockInterestTags,
  flocksUpcoming: flocksUpcoming,
  flocksUpcomingByCountry: flocksUpcomingByCountry,
  flocksUpcomingByLocality: flocksUpcomingByLocality,
  flockDetail: flockDetail,
  flocksByCurrentUser: flocksByCurrentUser,
  joinFlock: joinFlock,
  flockDialog: flockDialog,
  leaveFlock: leaveFlock,
  deleteFlock: deleteFlock,
  banFlockee: banFlockee,
  unbanFlockee: unbanFlockee
};
//# sourceMappingURL=apiFlocks.js.map