'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getJavascriptEnvironmentType = exports.JavascriptEnvironmentType = exports.getFileNameFromUri = exports.toString = exports.fieldsToString = undefined;

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Will omit undefined fields
function fieldsToString(fields) {
  var s = '';
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = (0, _getIterator3.default)((0, _keys2.default)(fields)), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var name = _step.value;

      var value = fields[name];
      if (value !== undefined) {
        s += name + ': ' + toString(value);
        s += '\n';
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  if (s.length > 0) {
    return '(' + s + ')';
  } else {
    return '';
  }
}

function toString(value) {
  return (0, _stringify2.default)(value);
}

function getFileNameFromUri(uri) {
  var parts = uri.split('/');
  return parts[parts.length - 1];
}

function getJavascriptEnvironmentType() {
  if (typeof document != 'undefined') {
    return JavascriptEnvironmentType.WEB;
  } else if (typeof navigator != 'undefined' && navigator.product == 'ReactNative') {
    return JavascriptEnvironmentType.REACT_NATIVE;
  } else {
    return JavascriptEnvironmentType.NODE;
  }
}

var JavascriptEnvironmentType = {
  WEB: 'WEB',
  REACT_NATIVE: 'REACT_NATIVE',
  NODE: 'NODE'
};

exports.fieldsToString = fieldsToString;
exports.toString = toString;
exports.getFileNameFromUri = getFileNameFromUri;
exports.JavascriptEnvironmentType = JavascriptEnvironmentType;
exports.getJavascriptEnvironmentType = getJavascriptEnvironmentType;
//# sourceMappingURL=helpers.js.map