'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.invalidateObject = invalidateObject;
exports.invalidateQuery = invalidateQuery;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _escapeRegexString = require('escape-regex-string');

var _escapeRegexString2 = _interopRequireDefault(_escapeRegexString);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ROOT_QUERY = 'ROOT_QUERY';

function invalidateObject(cacheData, objectName, objectId) {
  // Iterate through each item in cache data and delete all matches to object & its properties
  for (var itemKey in cacheData) {
    // Object name concat with its id must match
    if (!itemKey.match(new RegExp('^\\$?' + (0, _escapeRegexString2.default)(objectName) + '\\:' + (0, _escapeRegexString2.default)(objectId)))) {
      continue;
    }

    // Matching item, delete it
    _lodash2.default.unset(cacheData, itemKey);

    log('Invalidated \'' + itemKey + '\' in cache');
  }
}

function invalidateQuery(cacheData, queryName) {
  var queryProps = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  // Iterate through each item in cache data and delete any matches to query
  for (var itemKey in cacheData[ROOT_QUERY]) {
    // Query name must match
    if (!itemKey.match(new RegExp('^' + (0, _escapeRegexString2.default)(queryName) + '\\(?'))) {
      continue;
    }

    // All query props must match
    var itemQueryProps = {};
    var propsMatch = itemKey.match(/\(.*(\{.+\}).*\)/);
    if (propsMatch) {
      itemQueryProps = JSON.parse(propsMatch[1]);
    }

    for (var propKey in queryProps) {
      if (itemQueryProps[propKey] != queryProps[propKey]) {
        continue;
      }
    }

    // Matching item, delete it
    _lodash2.default.unset(cacheData[ROOT_QUERY], itemKey);

    log('Invalidated Query \'' + itemKey + '\' in cache');
  }
}

function log() {
  var _console;

  for (var _len = arguments.length, x = Array(_len), _key = 0; _key < _len; _key++) {
    x[_key] = arguments[_key];
  }

  (_console = console).log.apply(_console, ['[api/invalidation]'].concat(x));
}
//# sourceMappingURL=cacheInvalidation.js.map