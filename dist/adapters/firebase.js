'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.arrayMapToArray = exports.addToArrayMap = exports.arrayToArrayMap = exports.Firebase = undefined;

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _objectDestructuringEmpty2 = require('babel-runtime/helpers/objectDestructuringEmpty');

var _objectDestructuringEmpty3 = _interopRequireDefault(_objectDestructuringEmpty2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _firebase2 = require('firebase');

var _firebase3 = _interopRequireDefault(_firebase2);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _rxjs = require('rxjs');

var _rxjs2 = _interopRequireDefault(_rxjs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Firebase = function () {
  function Firebase() {
    (0, _classCallCheck3.default)(this, Firebase);
  }

  (0, _createClass3.default)(Firebase, [{
    key: 'init',
    value: function init(_ref) {
      var apiKey = _ref.apiKey,
          authDomain = _ref.authDomain,
          databaseUrl = _ref.databaseUrl,
          storageBucket = _ref.storageBucket;

      // Only initialise once
      if (this.app) {
        return;
      }

      this.app = _firebase3.default.initializeApp({
        apiKey: apiKey,
        authDomain: authDomain,
        databaseURL: databaseUrl,
        storageBucket: storageBucket
      });

      this._observables = [];
    }
  }, {
    key: 'signIn',
    value: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
        var sessionToken = _ref2.sessionToken;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _firebase3.default.auth().signInWithCustomToken(sessionToken);

              case 2:
                this._resetObservables();
                _context.next = 5;
                return this._onConnect.first().toPromise();

              case 5:
                return _context.abrupt('return', {
                  onConnect: this._onConnect,
                  onDisconnect: this._onDisconnect
                });

              case 6:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function signIn(_x) {
        return _ref3.apply(this, arguments);
      }

      return signIn;
    }()
  }, {
    key: 'signOut',
    value: function () {
      var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(_ref4) {
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                (0, _objectDestructuringEmpty3.default)(_ref4);
                _context2.next = 3;
                return _firebase3.default.auth().signOut();

              case 3:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function signOut(_x2) {
        return _ref5.apply(this, arguments);
      }

      return signOut;
    }()
  }, {
    key: 'getRef',
    value: function () {
      var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(_ref6) {
        var refPath = _ref6.refPath,
            _ref6$orderByChildPat = _ref6.orderByChildPath,
            orderByChildPath = _ref6$orderByChildPat === undefined ? null : _ref6$orderByChildPat,
            _ref6$limitFirst = _ref6.limitFirst,
            limitFirst = _ref6$limitFirst === undefined ? null : _ref6$limitFirst,
            _ref6$limitLast = _ref6.limitLast,
            limitLast = _ref6$limitLast === undefined ? null : _ref6$limitLast,
            _ref6$startAt = _ref6.startAt,
            startAt = _ref6$startAt === undefined ? null : _ref6$startAt,
            _ref6$endAt = _ref6.endAt,
            endAt = _ref6$endAt === undefined ? null : _ref6$endAt;
        var ref, query, value;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                ref = _firebase3.default.database().ref(refPath);
                query = this._sortLimit(ref, {
                  orderByChildPath: orderByChildPath,
                  limitFirst: limitFirst,
                  limitLast: limitLast,
                  startAt: startAt,
                  endAt: endAt
                });
                _context3.next = 4;
                return this._getQueryValue(query);

              case 4:
                value = _context3.sent;

                log('Retrieved value of ' + refPath, value);
                return _context3.abrupt('return', value);

              case 7:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function getRef(_x3) {
        return _ref7.apply(this, arguments);
      }

      return getRef;
    }()
  }, {
    key: 'subscribeToRef',
    value: function () {
      var _ref9 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(_ref8) {
        var _this = this;

        var refPath = _ref8.refPath,
            _ref8$orderByChildPat = _ref8.orderByChildPath,
            orderByChildPath = _ref8$orderByChildPat === undefined ? null : _ref8$orderByChildPat,
            _ref8$limitFirst = _ref8.limitFirst,
            limitFirst = _ref8$limitFirst === undefined ? null : _ref8$limitFirst,
            _ref8$limitLast = _ref8.limitLast,
            limitLast = _ref8$limitLast === undefined ? null : _ref8$limitLast,
            _ref8$startAt = _ref8.startAt,
            startAt = _ref8$startAt === undefined ? null : _ref8$startAt,
            _ref8$endAt = _ref8.endAt,
            endAt = _ref8$endAt === undefined ? null : _ref8$endAt;
        var onUpdate, ref;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                onUpdate = new _rxjs2.default.Subject();
                ref = _firebase3.default.database().ref(refPath);


                ref.on('value', function () {
                  var _ref10 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(snapshot) {
                    var query, value;
                    return _regenerator2.default.wrap(function _callee4$(_context4) {
                      while (1) {
                        switch (_context4.prev = _context4.next) {
                          case 0:
                            query = _this._sortLimit(snapshot.ref, {
                              orderByChildPath: orderByChildPath,
                              limitFirst: limitFirst,
                              limitLast: limitLast,
                              startAt: startAt,
                              endAt: endAt
                            });
                            _context4.next = 3;
                            return _this._getQueryValue(query);

                          case 3:
                            value = _context4.sent;

                            log('Notified of change in value of ' + refPath, value);
                            onUpdate.next(value);

                          case 6:
                          case 'end':
                            return _context4.stop();
                        }
                      }
                    }, _callee4, _this);
                  }));

                  return function (_x5) {
                    return _ref10.apply(this, arguments);
                  };
                }());

                this._observables.push(onUpdate);
                return _context5.abrupt('return', onUpdate);

              case 5:
              case 'end':
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function subscribeToRef(_x4) {
        return _ref9.apply(this, arguments);
      }

      return subscribeToRef;
    }()
  }, {
    key: 'setRef',
    value: function () {
      var _ref12 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(_ref11) {
        var refPath = _ref11.refPath,
            value = _ref11.value;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return _firebase3.default.database().ref(refPath).set(value);

              case 2:
                log('Set value of ' + refPath, value);

              case 3:
              case 'end':
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function setRef(_x6) {
        return _ref12.apply(this, arguments);
      }

      return setRef;
    }()
  }, {
    key: 'updateRef',
    value: function () {
      var _ref14 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7(_ref13) {
        var refPath = _ref13.refPath,
            value = _ref13.value;
        return _regenerator2.default.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return _firebase3.default.database().ref(refPath).update(value);

              case 2:
                log('Updated value of ' + refPath, value);

              case 3:
              case 'end':
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function updateRef(_x7) {
        return _ref14.apply(this, arguments);
      }

      return updateRef;
    }()
  }, {
    key: '_sortLimit',
    value: function _sortLimit(ref, _ref15) {
      var orderByChildPath = _ref15.orderByChildPath,
          limitFirst = _ref15.limitFirst,
          limitLast = _ref15.limitLast,
          startAt = _ref15.startAt,
          endAt = _ref15.endAt;

      var newRef = ref;
      if (orderByChildPath) {
        newRef = newRef.orderByChild(orderByChildPath);
      }
      if (startAt) {
        newRef = newRef.startAt(startAt);
      }
      if (endAt) {
        newRef = newRef.endAt(endAt);
      }
      if (limitFirst) {
        newRef = newRef.limitToFirst(limitFirst);
      }
      if (limitLast) {
        newRef = newRef.limitToLast(limitLast);
      }
      return newRef;
    }
  }, {
    key: '_resetObservables',
    value: function () {
      var _ref16 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8() {
        var _this2 = this;

        return _regenerator2.default.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                // Complete previous connections' observables
                [this._onConnect, this._onDisconnect].concat((0, _toConsumableArray3.default)(this._observables)).forEach(function (x) {
                  if (x) {
                    x.complete();
                  }
                });

                this._onConnect = new _rxjs2.default.Subject();
                this._onDisconnect = new _rxjs2.default.Subject();
                this._observables = [];

                // Ensure that observers get attached before observable emits
                setTimeout(function () {
                  var connectedRef = _firebase3.default.database().ref('.info/connected');
                  connectedRef.on('value', function (snap) {
                    if (snap.val()) {
                      log('Connected to Firebase Database.');
                      _this2._onConnect.next();
                    } else {
                      log('Disconnected from Firebase Database.');
                      _this2._onDisconnect.next();
                    }
                  });
                });

              case 5:
              case 'end':
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function _resetObservables() {
        return _ref16.apply(this, arguments);
      }

      return _resetObservables;
    }()
  }, {
    key: '_getQueryValue',
    value: function () {
      var _ref17 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9(query) {
        return _regenerator2.default.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.next = 2;
                return new _promise2.default(function (resolve, reject) {
                  query.once('value', function (snapshot) {
                    var value = snapshot.val();
                    if (value === null) {
                      value = {}; // Standardise all `nulls` to empty maps
                    }
                    resolve(value);
                  }, function (error) {
                    console.error(error);
                    reject(error);
                  });
                });

              case 2:
                return _context9.abrupt('return', _context9.sent);

              case 3:
              case 'end':
                return _context9.stop();
            }
          }
        }, _callee9, this);
      }));

      function _getQueryValue(_x8) {
        return _ref17.apply(this, arguments);
      }

      return _getQueryValue;
    }()
  }]);
  return Firebase;
}();

function log() {
  var _console;

  for (var _len = arguments.length, message = Array(_len), _key = 0; _key < _len; _key++) {
    message[_key] = arguments[_key];
  }

  (_console = console).log.apply(_console, ['[adapters/firebase]'].concat(message));
}

function arrayToArrayMap(array) {
  return _lodash2.default.mapValues(_lodash2.default.keyBy(array), function () {
    return true;
  });
}

function addToArrayMap(map, value) {
  map[value] = true;
  return map;
}

function arrayMapToArray(map) {
  return (0, _keys2.default)(map);
}

var firebase = new Firebase();
exports.default = firebase;
exports.Firebase = Firebase;
exports.arrayToArrayMap = arrayToArrayMap;
exports.addToArrayMap = addToArrayMap;
exports.arrayMapToArray = arrayMapToArray;
//# sourceMappingURL=firebase.js.map