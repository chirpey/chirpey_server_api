'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var sendMessageInDialog = function () {
  var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref) {
    var dialogUserId = _ref.dialogUserId,
        dialogId = _ref.dialogId,
        text = _ref.text;

    var message, _ref3, _ref4;

    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            (0, _assert2.default)(dialogId);
            (0, _assert2.default)(dialogUserId);
            (0, _assert2.default)(text);

            _context.next = 5;
            return _dialogManager2.default.sendMessageInDialog({
              dialogId: dialogId,
              dialogUserId: dialogUserId,
              text: text
            });

          case 5:
            message = _context.sent;
            _context.next = 8;
            return populateUsersInMessages([message]);

          case 8:
            _ref3 = _context.sent;
            _ref4 = (0, _slicedToArray3.default)(_ref3, 1);
            message = _ref4[0];
            return _context.abrupt('return', message);

          case 12:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function sendMessageInDialog(_x) {
    return _ref2.apply(this, arguments);
  };
}();

var sendMessageInDialogViaServer = function () {
  var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(_ref5) {
    var dialogId = _ref5.dialogId,
        userId = _ref5.userId,
        text = _ref5.text;

    var _ref7, message, _ref8, _ref9;

    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            (0, _assert2.default)(userId);
            (0, _assert2.default)(dialogId);
            (0, _assert2.default)(text);

            _context2.next = 5;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject, (0, _helpers.toString)(userId), (0, _helpers.toString)(dialogId), (0, _helpers.toString)(text)));

          case 5:
            _ref7 = _context2.sent;
            message = _ref7.sendMessageInDialog;
            _context2.next = 9;
            return populateUsersInMessages([message]);

          case 9:
            _ref8 = _context2.sent;
            _ref9 = (0, _slicedToArray3.default)(_ref8, 1);
            message = _ref9[0];
            return _context2.abrupt('return', message);

          case 13:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function sendMessageInDialogViaServer(_x2) {
    return _ref6.apply(this, arguments);
  };
}();

var getMessagesInDialog = function () {
  var _ref11 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(_ref10) {
    var dialogId = _ref10.dialogId,
        _ref10$limit = _ref10.limit,
        limit = _ref10$limit === undefined ? 100 : _ref10$limit,
        _ref10$offset = _ref10.offset,
        offset = _ref10$offset === undefined ? 0 : _ref10$offset;
    var messages;
    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            (0, _assert2.default)(dialogId);

            _context3.next = 3;
            return _dialogManager2.default.getMessagesInDialog({
              dialogId: dialogId,
              limit: limit,
              offset: offset
            });

          case 3:
            messages = _context3.sent;
            _context3.next = 6;
            return populateUsersInMessages(messages);

          case 6:
            messages = _context3.sent;
            return _context3.abrupt('return', messages);

          case 8:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));

  return function getMessagesInDialog(_x3) {
    return _ref11.apply(this, arguments);
  };
}();

var createDialogSession = function () {
  var _ref13 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(_ref12) {
    var userId = _ref12.userId;

    var _ref14, _ref14$createDialogSe, fbSessionToken, dialogUserId, _ref15, onDisconnect, onConnect, _ref16, onMessage;

    return _regenerator2.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            (0, _assert2.default)(userId);

            // Generate dialog session token on server
            _context4.next = 3;
            return _baseApi2.default.mutate((0, _graphqlTag2.default)(_templateObject2, (0, _helpers.toString)(userId)));

          case 3:
            _ref14 = _context4.sent;
            _ref14$createDialogSe = _ref14.createDialogSession;
            fbSessionToken = _ref14$createDialogSe.fbSessionToken;
            dialogUserId = _ref14$createDialogSe.dialogUserId;
            _context4.next = 9;
            return _dialogManager2.default.createSession({
              sessionToken: fbSessionToken
            });

          case 9:
            _ref15 = _context4.sent;
            onDisconnect = _ref15.onDisconnect;
            onConnect = _ref15.onConnect;
            _context4.next = 14;
            return _dialogManager2.default.getDialogUserEvents({
              dialogUserId: dialogUserId
            });

          case 14:
            _ref16 = _context4.sent;
            onMessage = _ref16.onMessage;
            return _context4.abrupt('return', {
              dialogUserId: dialogUserId,
              onDisconnect: onDisconnect,
              onConnect: onConnect,
              onMessage: onMessage
            });

          case 17:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, this);
  }));

  return function createDialogSession(_x4) {
    return _ref13.apply(this, arguments);
  };
}();

var destroyDialogSession = function () {
  var _ref17 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5() {
    return _regenerator2.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return _dialogManager2.default.destroySession({});

          case 2:
          case 'end':
            return _context5.stop();
        }
      }
    }, _callee5, this);
  }));

  return function destroyDialogSession() {
    return _ref17.apply(this, arguments);
  };
}();

var populateUsersInMessages = function () {
  var _ref18 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(messages) {
    var dialogUserIds, _ref19, users, usersByDialogUserIds;

    return _regenerator2.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            // Get users for each message
            dialogUserIds = _lodash2.default.uniq(messages.map(function (x) {
              return x.senderId;
            }));
            _context6.next = 3;
            return _baseApi2.default.query((0, _graphqlTag2.default)(_templateObject3, (0, _helpers.toString)(dialogUserIds)));

          case 3:
            _ref19 = _context6.sent;
            users = _ref19.usersByDialogUserIds;
            usersByDialogUserIds = _lodash2.default.keyBy(users, 'dialogUserId');

            messages.forEach(function (x) {
              x.sender = usersByDialogUserIds[x.senderId];
            });
            return _context6.abrupt('return', messages);

          case 8:
          case 'end':
            return _context6.stop();
        }
      }
    }, _callee6, this);
  }));

  return function populateUsersInMessages(_x5) {
    return _ref18.apply(this, arguments);
  };
}();

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        sendMessageInDialog(\n          userId: ', '\n          dialogId: ', '\n          text: ', '\n        ) {\n          id\n          text\n          senderId\n          sentAt\n        }\n      }\n    '], ['\n      mutation {\n        sendMessageInDialog(\n          userId: ', '\n          dialogId: ', '\n          text: ', '\n        ) {\n          id\n          text\n          senderId\n          sentAt\n        }\n      }\n    ']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n      mutation {\n        createDialogSession(\n          userId: ', '\n        ){\n          fbSessionToken\n          dialogUserId\n        }\n      }\n    '], ['\n      mutation {\n        createDialogSession(\n          userId: ', '\n        ){\n          fbSessionToken\n          dialogUserId\n        }\n      }\n    ']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n      query {\n        usersByDialogUserIds(dialogUserIds: ', ') {\n          id\n          firstName\n          lastName\n          profileImage {\n            id\n            path\n          }\n          dialogUserId\n        }\n      }\n    '], ['\n      query {\n        usersByDialogUserIds(dialogUserIds: ', ') {\n          id\n          firstName\n          lastName\n          profileImage {\n            id\n            path\n          }\n          dialogUserId\n        }\n      }\n    ']);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _baseApi = require('./baseApi');

var _baseApi2 = _interopRequireDefault(_baseApi);

var _dialogManager = require('./utils/dialogManager');

var _dialogManager2 = _interopRequireDefault(_dialogManager);

var _helpers = require('./helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  sendMessageInDialog: sendMessageInDialog,
  sendMessageInDialogViaServer: sendMessageInDialogViaServer,
  getMessagesInDialog: getMessagesInDialog,
  createDialogSession: createDialogSession,
  destroyDialogSession: destroyDialogSession
};
//# sourceMappingURL=apiDialog.js.map