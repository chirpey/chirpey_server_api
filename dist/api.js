'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _apiDev = require('./apiDev');

var _apiDev2 = _interopRequireDefault(_apiDev);

var _apiDialog = require('./apiDialog');

var _apiDialog2 = _interopRequireDefault(_apiDialog);

var _apiCommon = require('./apiCommon');

var _apiCommon2 = _interopRequireDefault(_apiCommon);

var _apiUsers = require('./apiUsers');

var _apiUsers2 = _interopRequireDefault(_apiUsers);

var _apiFlocks = require('./apiFlocks');

var _apiFlocks2 = _interopRequireDefault(_apiFlocks);

var _baseApi = require('./baseApi');

var _baseApi2 = _interopRequireDefault(_baseApi);

var _firebase = require('./adapters/firebase');

var _firebase2 = _interopRequireDefault(_firebase);

var _package = require('../package.json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _extends3.default)({
  getVersion: function getVersion() {
    return _package.version;
  },
  init: function init(_ref) {
    var serverHost = _ref.serverHost,
        serverPort = _ref.serverPort,
        firebaseApiKey = _ref.firebaseApiKey,
        firebaseAuthDomain = _ref.firebaseAuthDomain,
        firebaseDatabaseUrl = _ref.firebaseDatabaseUrl,
        firebaseStorageBucket = _ref.firebaseStorageBucket,
        graphqlPath = _ref.graphqlPath,
        customFetch = _ref.customFetch;

    _baseApi2.default.init({
      serverHost: serverHost,
      serverPort: serverPort,
      graphqlPath: graphqlPath,
      customFetch: customFetch
    });
    _firebase2.default.init({
      apiKey: firebaseApiKey,
      authDomain: firebaseAuthDomain,
      databaseUrl: firebaseDatabaseUrl,
      storageBucket: firebaseStorageBucket
    });
  },
  setUserToken: function setUserToken() {
    return _baseApi2.default.setUserToken.apply(_baseApi2.default, arguments);
  },
  getUserToken: function getUserToken() {
    return _baseApi2.default.getUserToken.apply(_baseApi2.default, arguments);
  },
  getUrl: function getUrl() {
    return _baseApi2.default.getUrl.apply(_baseApi2.default, arguments);
  }
}, _apiCommon2.default, _apiDev2.default, _apiDialog2.default, _apiUsers2.default, _apiFlocks2.default);
//# sourceMappingURL=api.js.map