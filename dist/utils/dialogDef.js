'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _uuid = require('uuid');

var _uuid2 = _interopRequireDefault(_uuid);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _firebase = require('firebase');

var _firebase2 = _interopRequireDefault(_firebase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KEY_DIALOGS = 'dialogs';
var KEY_DIALOG_USERS = 'dialogUsers';

function makeDialogUser(_ref) {
  var id = _ref.id;

  (0, _assert2.default)(id);

  var object = makeObject({ id: id });
  return (0, _extends3.default)({}, object, {
    dialogIds: {}
  });
}

function makeDialog(_ref2) {
  var id = _ref2.id;

  (0, _assert2.default)(id);

  var object = makeObject({ id: id });
  return (0, _extends3.default)({}, object, {
    userIds: {},
    messages: {}
  });
}

function makeDialogMessage(_ref3) {
  var text = _ref3.text,
      senderId = _ref3.senderId;

  (0, _assert2.default)(text);
  (0, _assert2.default)(senderId);

  var object = makeObject({});

  return (0, _extends3.default)({}, object, {
    sentAt: _firebase2.default.database.ServerValue.TIMESTAMP,
    text: text,
    senderId: senderId
  });
}

function makeObject(_ref4) {
  var _ref4$id = _ref4.id,
      id = _ref4$id === undefined ? _uuid2.default.v4() : _ref4$id;

  return {
    id: id,
    createdAt: _firebase2.default.database.ServerValue.TIMESTAMP,
    deletedAt: null
  };
}

function deleteObject(object) {
  return (0, _extends3.default)({}, object, {
    deletedAt: _firebase2.default.database.ServerValue.TIMESTAMP
  });
}

function processTimestampsOfMessage(message) {
  message.sentAt = (0, _moment2.default)(message.sentAt).toISOString();
  return message;
}

var RefPath = {
  DIALOG: function DIALOG(_ref5) {
    var dialogId = _ref5.dialogId;

    return '/' + KEY_DIALOGS + '/' + dialogId;
  },
  DIALOG_MESSAGE: function DIALOG_MESSAGE(_ref6) {
    var dialogId = _ref6.dialogId,
        messageId = _ref6.messageId;

    return RefPath.DIALOG_MESSAGES({ dialogId: dialogId }) + '/' + messageId;
  },
  DIALOG_MESSAGES: function DIALOG_MESSAGES(_ref7) {
    var dialogId = _ref7.dialogId;

    return RefPath.DIALOG({ dialogId: dialogId }) + '/messages';
  },
  DIALOG_USER_IDS: function DIALOG_USER_IDS(_ref8) {
    var dialogId = _ref8.dialogId;

    return RefPath.DIALOG({ dialogId: dialogId }) + '/userIds';
  },
  DIALOG_USER: function DIALOG_USER(_ref9) {
    var dialogUserId = _ref9.dialogUserId;

    return '/' + KEY_DIALOG_USERS + '/' + dialogUserId;
  },
  DIALOG_USER_DIALOG_IDS: function DIALOG_USER_DIALOG_IDS(_ref10) {
    var dialogUserId = _ref10.dialogUserId;

    return RefPath.DIALOG_USER({ dialogUserId: dialogUserId }) + '/dialogIds';
  }
};

exports.default = {
  makeDialog: makeDialog,
  makeDialogUser: makeDialogUser,
  makeDialogMessage: makeDialogMessage,
  processTimestampsOfMessage: processTimestampsOfMessage,
  deleteObject: deleteObject,
  RefPath: RefPath
};
//# sourceMappingURL=dialogDef.js.map