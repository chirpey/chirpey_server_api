'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _objectDestructuringEmpty2 = require('babel-runtime/helpers/objectDestructuringEmpty');

var _objectDestructuringEmpty3 = _interopRequireDefault(_objectDestructuringEmpty2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _rxjs = require('rxjs');

var _rxjs2 = _interopRequireDefault(_rxjs);

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _dialogDef = require('./dialogDef');

var _dialogDef2 = _interopRequireDefault(_dialogDef);

var _firebase = require('../adapters/firebase');

var _firebase2 = _interopRequireDefault(_firebase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DialogManager = function () {
  function DialogManager() {
    (0, _classCallCheck3.default)(this, DialogManager);
  }

  (0, _createClass3.default)(DialogManager, [{
    key: 'createSession',
    value: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref) {
        var sessionToken = _ref.sessionToken;

        var _ref3, onConnect, onDisconnect;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                (0, _assert2.default)(sessionToken);

                _context.next = 3;
                return _firebase2.default.signIn({ sessionToken: sessionToken });

              case 3:
                _ref3 = _context.sent;
                onConnect = _ref3.onConnect;
                onDisconnect = _ref3.onDisconnect;
                return _context.abrupt('return', {
                  onConnect: onConnect,
                  onDisconnect: onDisconnect
                });

              case 7:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function createSession(_x) {
        return _ref2.apply(this, arguments);
      }

      return createSession;
    }()
  }, {
    key: 'destroySession',
    value: function () {
      var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(_ref4) {
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                (0, _objectDestructuringEmpty3.default)(_ref4);
                _context2.next = 3;
                return _firebase2.default.signOut({});

              case 3:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function destroySession(_x2) {
        return _ref5.apply(this, arguments);
      }

      return destroySession;
    }()
  }, {
    key: 'sendMessageInDialog',
    value: function () {
      var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(_ref6) {
        var dialogUserId = _ref6.dialogUserId,
            dialogId = _ref6.dialogId,
            text = _ref6.text;
        var message, refPath;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                (0, _assert2.default)(dialogUserId);
                (0, _assert2.default)(dialogId);
                (0, _assert2.default)(text);

                message = _dialogDef2.default.makeDialogMessage({
                  senderId: dialogUserId,
                  text: text
                });
                refPath = _dialogDef2.default.RefPath.DIALOG_MESSAGE({
                  dialogId: dialogId,
                  messageId: message.id
                });
                _context3.next = 7;
                return _firebase2.default.updateRef({
                  refPath: refPath,
                  value: message
                });

              case 7:

                message = _dialogDef2.default.processTimestampsOfMessage(message);

                return _context3.abrupt('return', message);

              case 9:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function sendMessageInDialog(_x3) {
        return _ref7.apply(this, arguments);
      }

      return sendMessageInDialog;
    }()
  }, {
    key: 'getMessagesInDialog',
    value: function () {
      var _ref9 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(_ref8) {
        var dialogId = _ref8.dialogId,
            _ref8$limit = _ref8.limit,
            limit = _ref8$limit === undefined ? 100 : _ref8$limit,
            _ref8$offset = _ref8.offset,
            offset = _ref8$offset === undefined ? 0 : _ref8$offset;
        var refPath, messages;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                (0, _assert2.default)(dialogId);
                (0, _assert2.default)(_lodash2.default.isNumber(limit) && limit > 0);
                (0, _assert2.default)(_lodash2.default.isNumber(offset) && offset >= 0);

                refPath = _dialogDef2.default.RefPath.DIALOG_MESSAGES({ dialogId: dialogId });
                _context4.next = 6;
                return _firebase2.default.getRef({
                  refPath: refPath,
                  orderByChildPath: 'sentAt',
                  limitLast: limit,
                  endAt: offset
                });

              case 6:
                messages = _context4.sent;


                messages = (0, _values2.default)(messages);
                messages = _lodash2.default.sortBy(messages, function (x) {
                  return (0, _moment2.default)(x.sentAt).valueOf();
                }); // Sort in chronological order
                messages = messages.map(_dialogDef2.default.processTimestampsOfMessage);

                return _context4.abrupt('return', messages);

              case 11:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function getMessagesInDialog(_x4) {
        return _ref9.apply(this, arguments);
      }

      return getMessagesInDialog;
    }()
  }, {
    key: 'getDialogUserEvents',
    value: function () {
      var _ref11 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(_ref10) {
        var _this = this;

        var dialogUserId = _ref10.dialogUserId;
        var refPath, onDialogIdsChange, onMessage, onDialogChangeSubs;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                (0, _assert2.default)(dialogUserId);

                refPath = _dialogDef2.default.RefPath.DIALOG_USER_DIALOG_IDS({ dialogUserId: dialogUserId });
                _context6.next = 4;
                return _firebase2.default.subscribeToRef({ refPath: refPath });

              case 4:
                onDialogIdsChange = _context6.sent;
                onMessage = new _rxjs2.default.Subject();
                onDialogChangeSubs = [];

                onDialogIdsChange.subscribe({
                  next: function () {
                    var _ref12 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(dialogIds) {
                      var dialogsEvents;
                      return _regenerator2.default.wrap(function _callee5$(_context5) {
                        while (1) {
                          switch (_context5.prev = _context5.next) {
                            case 0:
                              // Unsubscribe from previous dialogs
                              onDialogChangeSubs.forEach(function (x) {
                                return x.unsubscribe();
                              });
                              _context5.next = 3;
                              return _promise2.default.all((0, _keys2.default)(dialogIds).map(function (x) {
                                return _this._getDialogEvents({ dialogId: x });
                              }));

                            case 3:
                              dialogsEvents = _context5.sent;

                              onDialogChangeSubs = dialogsEvents.map(function (_ref13) {
                                var dialogId = _ref13.dialogId,
                                    onDialogMessagesUpdate = _ref13.onDialogMessagesUpdate;

                                return onDialogMessagesUpdate.subscribe({
                                  next: function next(messages) {
                                    onMessage.next({
                                      dialogId: dialogId
                                    });
                                  }
                                });
                              });

                            case 5:
                            case 'end':
                              return _context5.stop();
                          }
                        }
                      }, _callee5, _this);
                    }));

                    function next(_x6) {
                      return _ref12.apply(this, arguments);
                    }

                    return next;
                  }()
                });

                return _context6.abrupt('return', {
                  onMessage: onMessage
                });

              case 9:
              case 'end':
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function getDialogUserEvents(_x5) {
        return _ref11.apply(this, arguments);
      }

      return getDialogUserEvents;
    }()
  }, {
    key: '_getDialogEvents',
    value: function () {
      var _ref15 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7(_ref14) {
        var dialogId = _ref14.dialogId;
        var refPath, onDialogMessagesUpdate;
        return _regenerator2.default.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                refPath = _dialogDef2.default.RefPath.DIALOG_MESSAGES({ dialogId: dialogId });
                _context7.next = 3;
                return _firebase2.default.subscribeToRef({ refPath: refPath });

              case 3:
                onDialogMessagesUpdate = _context7.sent;
                return _context7.abrupt('return', {
                  dialogId: dialogId,
                  onDialogMessagesUpdate: onDialogMessagesUpdate
                });

              case 5:
              case 'end':
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function _getDialogEvents(_x7) {
        return _ref15.apply(this, arguments);
      }

      return _getDialogEvents;
    }()
  }]);
  return DialogManager;
}();

var dialogManager = new DialogManager();
exports.default = dialogManager;
//# sourceMappingURL=dialogManager.js.map