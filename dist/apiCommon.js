'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var uploadImages = function () {
  var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref) {
    var images = _ref.images;

    var formData, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, image, imageData, result;

    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            formData = new _formData2.default();
            _iteratorNormalCompletion = true;
            _didIteratorError = false;
            _iteratorError = undefined;
            _context.prev = 4;

            for (_iterator = (0, _getIterator3.default)(images); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              image = _step.value;

              (0, _assert2.default)(image.uri);
              imageData = getImageData({
                imageUri: image.uri,
                imageType: image.type
              });

              formData.append('images', imageData);
            }

            _context.next = 12;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context['catch'](4);
            _didIteratorError = true;
            _iteratorError = _context.t0;

          case 12:
            _context.prev = 12;
            _context.prev = 13;

            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }

          case 15:
            _context.prev = 15;

            if (!_didIteratorError) {
              _context.next = 18;
              break;
            }

            throw _iteratorError;

          case 18:
            return _context.finish(15);

          case 19:
            return _context.finish(12);

          case 20:
            _context.next = 22;
            return _baseApi2.default.fetch('/upload', {
              method: 'POST',
              body: formData
            });

          case 22:
            result = _context.sent;
            return _context.abrupt('return', result);

          case 24:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[4, 8, 12, 20], [13,, 15, 19]]);
  }));

  return function uploadImages(_x) {
    return _ref2.apply(this, arguments);
  };
}();

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _formData = require('form-data');

var _formData2 = _interopRequireDefault(_formData);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _baseApi = require('./baseApi');

var _baseApi2 = _interopRequireDefault(_baseApi);

var _helpers = require('./helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getImageData(_ref3) {
  var imageUri = _ref3.imageUri,
      imageType = _ref3.imageType;

  var envType = (0, _helpers.getJavascriptEnvironmentType)();
  try {
    // `!envType` prevents code analysis that would pull up the import 'fs' in other JS environments
    var FS = !envType || 'fs';
    var fs = require(FS);
    var imageData = fs.createReadStream(imageUri);
    return imageData;
  } catch (error) {
    if (envType == _helpers.JavascriptEnvironmentType.REACT_NATIVE) {
      var type = imageType || inferTypeFromImageUri(imageUri) || undefined;
      var name = (0, _helpers.getFileNameFromUri)(imageUri);
      var _imageData = {
        uri: imageUri,
        type: type,
        name: name
      };
      return _imageData;
    } else {
      throw new Error('Environment type unsupported!');
    }
  }
}

function inferTypeFromImageUri(uri) {
  var ext = _path2.default.extname(uri);
  if (['.jpg', 'jpeg'].includes(ext)) {
    return 'image/jpeg';
  } else if (['.png'].includes(ext)) {
    return 'image/png';
  }
  // File extension not recognized
  return null;
}

exports.default = {
  uploadImages: uploadImages
};
//# sourceMappingURL=apiCommon.js.map