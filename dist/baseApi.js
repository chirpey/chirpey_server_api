'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BaseApi = undefined;

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _fetchEverywhere = require('fetch-everywhere');

var _fetchEverywhere2 = _interopRequireDefault(_fetchEverywhere);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _createApolloClient = require('./createApolloClient');

var _createApolloClient2 = _interopRequireDefault(_createApolloClient);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BaseApi = function () {
  function BaseApi() {
    (0, _classCallCheck3.default)(this, BaseApi);
  }

  (0, _createClass3.default)(BaseApi, [{
    key: 'init',
    value: function init(_ref) {
      var _this = this;

      var serverHost = _ref.serverHost,
          serverPort = _ref.serverPort,
          _ref$graphqlPath = _ref.graphqlPath,
          graphqlPath = _ref$graphqlPath === undefined ? '/graphql' : _ref$graphqlPath,
          _ref$customFetch = _ref.customFetch,
          customFetch = _ref$customFetch === undefined ? _fetchEverywhere2.default : _ref$customFetch;

      this.customFetch = customFetch;
      this.serverHost = serverHost;
      this.serverPort = serverPort;
      this.token = null;
      this.isInited = true;
      this.apolloClient = (0, _createApolloClient2.default)({
        getUserToken: function getUserToken() {
          return _this.token;
        },
        serverHost: serverHost,
        serverPort: serverPort,
        graphqlPath: graphqlPath,
        customFetch: customFetch
      });
    }
  }, {
    key: 'setUserToken',
    value: function setUserToken(token) {
      // Has token changed?
      if (token == this.token) {
        return;
      }
      this.token = token;
    }
  }, {
    key: 'getUserToken',
    value: function getUserToken() {
      return this.token;
    }
  }, {
    key: 'query',
    value: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(graphqlQuery) {
        var _ref3 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
            useCache = _ref3.useCache;

        var sourceBody, _ref4, data, error;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                (0, _assert2.default)(this.isInited);

                sourceBody = _lodash2.default.get(graphqlQuery, 'loc.source.body');

                log('Sending GraphQL query ' + (useCache ? '(cache-first)' : ''), graphqlQuery, sourceBody ? ' with source body: \n```' + sourceBody + '\n```' : '');

                _context.prev = 3;
                _context.next = 6;
                return this.apolloClient.query({
                  query: graphqlQuery,
                  fetchPolicy: useCache ? 'cache-first' : 'network-only'
                });

              case 6:
                _ref4 = _context.sent;
                data = _ref4.data;


                log('Received result of GraphQL query', getObjectPrettyPrint(data));

                return _context.abrupt('return', data);

              case 12:
                _context.prev = 12;
                _context.t0 = _context['catch'](3);

                log('Error occured during GraphQL query', _context.t0);
                error = processGqlError(_context.t0);
                throw error;

              case 17:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[3, 12]]);
      }));

      function query(_x2) {
        return _ref2.apply(this, arguments);
      }

      return query;
    }()
  }, {
    key: 'mutate',
    value: function () {
      var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(graphqlMutation) {
        var sourceBody, _ref6, data, error;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                (0, _assert2.default)(this.isInited);

                sourceBody = _lodash2.default.get(graphqlMutation, 'loc.source.body');

                log('Sending GraphQL mutation', graphqlMutation, sourceBody ? ' with source body: \n```' + sourceBody + '\n```' : '');

                _context2.prev = 3;
                _context2.next = 6;
                return this.apolloClient.mutate({
                  mutation: graphqlMutation
                });

              case 6:
                _ref6 = _context2.sent;
                data = _ref6.data;


                log('Received result of GraphQL mutation', getObjectPrettyPrint(data));
                return _context2.abrupt('return', data);

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2['catch'](3);

                log('Error occured during GraphQL mutation', _context2.t0);
                error = processGqlError(_context2.t0);
                throw error;

              case 17:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this, [[3, 12]]);
      }));

      function mutate(_x3) {
        return _ref5.apply(this, arguments);
      }

      return mutate;
    }()
  }, {
    key: 'fetch',
    value: function () {
      var _ref8 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(path, _ref7) {
        var method = _ref7.method,
            body = _ref7.body;
        var host, port, url, headers, response, resultJson, result, error;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                (0, _assert2.default)(this.isInited);

                host = this.serverHost;
                port = this.serverPort;
                url = 'http://' + host + ':' + port + path;


                log('Sending', method, url, 'with body', body, '...');

                headers = {};
                _context3.prev = 6;
                _context3.next = 9;
                return this.customFetch(url, { method: method, body: body, headers: headers });

              case 9:
                response = _context3.sent;
                _context3.next = 12;
                return response.text();

              case 12:
                resultJson = _context3.sent;
                result = null;
                _context3.prev = 14;

                result = JSON.parse(resultJson);
                _context3.next = 22;
                break;

              case 18:
                _context3.prev = 18;
                _context3.t0 = _context3['catch'](14);

                // Figure error from raw response text
                _context3.t0 = getErrorFromResponse(resultJson);
                throw _context3.t0;

              case 22:
                log('Result of HTTP request:', getObjectPrettyPrint(result));

                // All server responses come with `success` flag

                if (result.success) {
                  _context3.next = 25;
                  break;
                }

                throw new Error(result.error.message);

              case 25:
                return _context3.abrupt('return', result);

              case 28:
                _context3.prev = 28;
                _context3.t1 = _context3['catch'](6);

                log('Error occured during HTTP request', _context3.t1);
                throw _context3.t1;

              case 32:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this, [[6, 28], [14, 18]]);
      }));

      function fetch(_x4, _x5) {
        return _ref8.apply(this, arguments);
      }

      return fetch;
    }()
  }, {
    key: 'getUrl',
    value: function getUrl(filePath) {
      (0, _assert2.default)(this.isInited);
      if (!filePath) {
        return null;
      }
      return 'http://' + this.serverHost + ':' + this.serverPort + filePath;
    }
  }]);
  return BaseApi;
}();

function processGqlError(gqlError) {
  if (gqlError.networkError && gqlError.networkError.result && gqlError.networkError.result.errors && gqlError.networkError.result.errors.length > 0) {
    // Return GraphQL interface network errors as single user-friendly message
    gqlError.message = gqlError.networkError.result.errors[0].message;
  } else if (gqlError.graphQLErrors && gqlError.graphQLErrors.length > 0) {
    // Return GraphQL resolver errors clearly as single user-friendly message
    gqlError.message = gqlError.graphQLErrors[0].message;
    // Remove mention of "GraphQL" to forward error message to client
    gqlError.message = gqlError.message.replace(/^GraphQL\serror:\s/, '');
  } else {
    // Default error message
    gqlError.message = 'Error connecting to server.';
  }

  return gqlError;
}

function getErrorFromResponse(responseText) {
  if (new RegExp('<title>413 Request Entity Too Large</title>').exec(responseText)) {
    return new Error('Your file is too large to upload.');
  }

  log('Server error:', responseText);
  return new Error('Sorry, a server error occured...');
}

function log() {
  var _console;

  for (var _len = arguments.length, x = Array(_len), _key = 0; _key < _len; _key++) {
    x[_key] = arguments[_key];
  }

  (_console = console).log.apply(_console, ['[api/baseApi]'].concat(x));
}

function getObjectPrettyPrint(object) {
  return '\n```\n' + (0, _stringify2.default)(object, null, 2) + '\n```';
}

var baseApi = new BaseApi();
exports.default = baseApi;
exports.BaseApi = BaseApi;
//# sourceMappingURL=baseApi.js.map