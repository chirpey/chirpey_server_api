'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _apolloLink = require('apollo-link');

var _fetchEverywhere = require('fetch-everywhere');

var _fetchEverywhere2 = _interopRequireDefault(_fetchEverywhere);

var _apolloLinkHttp = require('apollo-link-http');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createHttpLink(_ref) {
  var serverHost = _ref.serverHost,
      serverPort = _ref.serverPort,
      graphqlPath = _ref.graphqlPath,
      getUserToken = _ref.getUserToken,
      _ref$customFetch = _ref.customFetch,
      customFetch = _ref$customFetch === undefined ? _fetchEverywhere2.default : _ref$customFetch;

  var endpoint = 'http://' + serverHost + ':' + serverPort + graphqlPath;
  log('Using GraphQL server endpoint:', endpoint);

  var httpLink = new _apolloLinkHttp.HttpLink({
    uri: endpoint,
    fetch: customFetch
  });

  var httpMiddlewareLink = new _apolloLink.ApolloLink(function (operation, forward) {
    // Log request made
    log('Sending GraphQL request:', operation);

    var headers = {};

    // Header to authenticate user with user token
    var userToken = getUserToken();
    if (userToken) {
      headers.Authorization = 'Bearer ' + userToken;
    }

    operation.setContext({
      headers: headers
    });

    return forward(operation).map(function (data) {
      // Log response
      log('Received GraphQL response:', data);
      return data;
    });
  });

  var link = httpMiddlewareLink.concat(httpLink);
  return link;
}

function log() {
  // console.log('[api/createHttpLink]', ...x);
}

exports.default = createHttpLink;
//# sourceMappingURL=createHttpLink.js.map