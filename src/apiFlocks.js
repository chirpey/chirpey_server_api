import gql from 'graphql-tag';

import { toString, fieldsToString } from './helpers';
import baseApi from './baseApi';

async function createFlock({
  ownerId,
  name,
  description,
  mapLocationLatitude,
  mapLocationLongitude,
  mapLocationName,
  mapLocationAddress,
  dateStart,
  dateEnd,
  interestTags,
  imageIds
}) {
  const { createFlock: { id, name: newName } } = await baseApi.mutate(
    gql`
      mutation {
        createFlock
          ${fieldsToString({
            ownerId,
            name,
            description,
            mapLocationLatitude,
            mapLocationLongitude,
            mapLocationName,
            mapLocationAddress,
            dateStart,
            dateEnd,
            interestTags,
            imageIds
          })}
        {
          id
          name
        }
      }
    `
  );

  return { id, name: newName };
}

async function updateFlock({
  flockId,
  currentOwnerId,
  name,
  newOwnerId,
  description,
  mapLocationLatitude,
  mapLocationLongitude,
  mapLocationName,
  mapLocationAddress,
  dateStart,
  dateEnd,
  interestTags,
  imageIds
}) {
  const { updateFlock: { id, name: newName } } = await baseApi.mutate(
    gql`
      mutation {
        updateFlock
          ${fieldsToString({
            flockId,
            currentOwnerId,
            name,
            newOwnerId,
            description,
            mapLocationLatitude,
            mapLocationLongitude,
            mapLocationName,
            mapLocationAddress,
            dateStart,
            dateEnd,
            interestTags,
            imageIds
          })}
        {
          id
          name
        }
      }
    `
  );

  return { id, name: newName };
}

async function deleteFlock({ flockId }) {
  const { deleteFlock: id } = await baseApi.mutate(
    gql`
      mutation {
        deleteFlock(
          flockId: ${toString(flockId)}
        )
      }
    `
  );

  return { id };
}

async function joinFlock({ flockId, userId }) {
  const { joinFlock: { id, name } } = await baseApi.mutate(
    gql`
      mutation {
        joinFlock(
          flockId: ${toString(flockId)}
          userId: ${toString(userId)}
        ) {
          id
          name
        }
      }
    `
  );

  return { id, name };
}

async function leaveFlock({ flockId, userId }) {
  const { leaveFlock: { id, name } } = await baseApi.mutate(
    gql`
      mutation {
        leaveFlock(
          flockId: ${toString(flockId)}
          userId: ${toString(userId)}
        ) {
          id
          name
        }
      }
    `
  );

  return { id, name };
}

async function banFlockee({ flockId, userId }) {
  const { banFlockee: { id, name } } = await baseApi.mutate(
    gql`
      mutation {
        banFlockee(
          flockId: ${toString(flockId)}
          userId: ${toString(userId)}
        ) {
          id
          name
        }
      }
    `
  );

  return { id, name };
}

async function unbanFlockee({ flockId, userId }) {
  const { unbanFlockee: { id, name } } = await baseApi.mutate(
    gql`
      mutation {
        unbanFlockee(
          flockId: ${toString(flockId)}
          userId: ${toString(userId)}
        ) {
          id
          name
        }
      }
    `
  );

  return { id, name };
}

// TODO: Have dynamic tags
async function flockInterestTags() {
  return ['Food', 'Adventure', 'Relaxation', 'Sightseeing', 'Urban'];
}

async function flocksByCurrentUser() {
  const { currentUser: { flocks } } = await baseApi.query(
    gql`
      query {
        currentUser {
          id
          flocks {
            id
            name
            noOfFlockees
            mapLocation {
              locality
              country
              address
            }
            dateStart
            dateEnd
            interestTags
            images {
              id
              path
            }
            owner {
              id
              firstName
              lastName
            }
          }
        }
      }
    `
  );

  return flocks;
}

async function flocksUpcoming({
  offset = 0,
  limit = 25,
  filterByCountries,
  filterByInterestTags,
  filterByDateStart,
  filterByDateEnd
}) {
  const { flocksUpcoming: flocks } = await baseApi.query(
    gql`
      query {
        flocksUpcoming
          ${fieldsToString({
            offset: offset || 0,
            limit: limit || 25,
            filterByCountries,
            filterByInterestTags,
            filterByDateStart,
            filterByDateEnd
          })}
        {
          id
          name
          noOfFlockees
          mapLocation {
            locality
            country
            address
          }
          dateStart
          dateEnd
          interestTags
          images {
            id
            path
          }
          owner {
            id
            firstName
            lastName
            profileImage {
              id
              path
            }
          }
        }
      }
    `
  );

  return flocks;
}

async function flocksUpcomingByLocality({
  offset = 0,
  limit = 25,
  flocksOffset = 0,
  flocksLimit = 25
}) {
  const { flockLocalities } = await baseApi.query(
    gql`
      query {
        flockLocalities(
          offset: ${offset}
          limit: ${limit}
        ) {
          country
          locality
          noOfFlockeesInFlocksUpcoming
          flocksUpcoming(
            offset: ${flocksOffset}
            limit: ${flocksLimit}
          ) {
            id
            name
            noOfFlockees
            mapLocation {
              locality
              country
              address
            }
            dateStart
            dateEnd
            interestTags
            images {
              id
              path
            }
            owner {
              id
              firstName
              lastName
            }
          }
        }
      }
    `
  );

  return flockLocalities;
}

async function flocksUpcomingByCountry({
  offset = 0,
  limit = 25,
  flocksOffset = 0,
  flocksLimit = 25
}) {
  const { flockCountries } = await baseApi.query(
    gql`
      query {
        flockCountries(
          offset: ${offset}
          limit: ${limit}
        ) {
          country
          noOfFlockeesInFlocksUpcoming
          flocksUpcoming(
            offset: ${flocksOffset}
            limit: ${flocksLimit}
          ) {
            id
            name
            noOfFlockees
            mapLocation {
              locality
              country
              address
            }
            dateStart
            dateEnd
            interestTags
            images {
              id
              path
            }
            owner {
              id
              firstName
              lastName
            }
          }
        }
      }
    `
  );

  return flockCountries;
}

async function flockDetail({ flockId }) {
  const { flocks: [flock] } = await baseApi.query(
    gql`
      query {
        flocks(
          ids: [${toString(flockId)}]
        ) {
          id
          name
          description
          noOfFlockees
          mapLocation {
            name
            country
            locality
            address
            latitude
            longitude
          }
          dateStart
          dateEnd
          interestTags
          images {
            id
            path
          }
          owner {
            id
            firstName
            lastName
            profileImage {
              id
              path
            }
          }
          flockees {
            id
            firstName
            lastName
            profileImage {
              id
              path
            }
          }
          bannedFlockees {
            id
            firstName
            lastName
            profileImage {
              id
              path
            }
          }
        }
      }
    `
  );

  return flock;
}

async function flockDialog({ flockId }) {
  const { flocks: [flock] } = await baseApi.query(
    gql`
      query {
        flocks(
          ids: [${toString(flockId)}]
        ) {
          dialogId
        }
      }
    `
  );

  return flock;
}

export default {
  createFlock,
  updateFlock,
  flockInterestTags,
  flocksUpcoming,
  flocksUpcomingByCountry,
  flocksUpcomingByLocality,
  flockDetail,
  flocksByCurrentUser,
  joinFlock,
  flockDialog,
  leaveFlock,
  deleteFlock,
  banFlockee,
  unbanFlockee
};
