// Will omit undefined fields
function fieldsToString(fields) {
  let s = '';
  for (const name of Object.keys(fields)) {
    const value = fields[name];
    if (value !== undefined) {
      s += `${name}: ${toString(value)}`;
      s += '\n';
    }
  }
  if (s.length > 0) {
    return `(${s})`;
  } else {
    return '';
  }
}

function toString(value) {
  return JSON.stringify(value);
}

function getFileNameFromUri(uri) {
  const parts = uri.split('/');
  return parts[parts.length - 1];
}

function getJavascriptEnvironmentType() {
  if (typeof document != 'undefined') {
    return JavascriptEnvironmentType.WEB;
  } else if (
    typeof navigator != 'undefined' &&
    navigator.product == 'ReactNative'
  ) {
    return JavascriptEnvironmentType.REACT_NATIVE;
  } else {
    return JavascriptEnvironmentType.NODE;
  }
}

const JavascriptEnvironmentType = {
  WEB: 'WEB',
  REACT_NATIVE: 'REACT_NATIVE',
  NODE: 'NODE'
};

export {
  fieldsToString,
  toString,
  getFileNameFromUri,
  JavascriptEnvironmentType,
  getJavascriptEnvironmentType
};
