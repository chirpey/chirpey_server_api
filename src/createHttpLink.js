import { ApolloLink } from 'apollo-link';
import fetch from 'fetch-everywhere';
import { HttpLink } from 'apollo-link-http';

function createHttpLink({
  serverHost,
  serverPort,
  graphqlPath,
  getUserToken,
  customFetch = fetch
}) {
  const endpoint = `http://${serverHost}:${serverPort}${graphqlPath}`;
  log('Using GraphQL server endpoint:', endpoint);

  const httpLink = new HttpLink({
    uri: endpoint,
    fetch: customFetch
  });

  const httpMiddlewareLink = new ApolloLink((operation, forward) => {
    // Log request made
    log('Sending GraphQL request:', operation);

    const headers = {};

    // Header to authenticate user with user token
    const userToken = getUserToken();
    if (userToken) {
      headers.Authorization = `Bearer ${userToken}`;
    }

    operation.setContext({
      headers
    });

    return forward(operation).map(data => {
      // Log response
      log('Received GraphQL response:', data);
      return data;
    });
  });

  const link = httpMiddlewareLink.concat(httpLink);
  return link;
}

function log(...x) {
  // console.log('[api/createHttpLink]', ...x);
}

export default createHttpLink;
