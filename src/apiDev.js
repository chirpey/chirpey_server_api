import gql from 'graphql-tag';
import baseApi from './baseApi';
import { fieldsToString } from './helpers';

async function isRunning() {
  const { healthTest: { isRunning } } = await baseApi.mutate(
    gql`
      mutation {
        healthTest {
          isRunning
        }
      }
    `
  );

  return isRunning;
}

async function triggerEveryMinuteTask() {
  await baseApi.mutate(
    gql`
      mutation {
        triggerEveryMinuteTask
      }
    `
  );
}

async function addThrowErrorTask() {
  await baseApi.mutate(
    gql`
      mutation {
        addThrowErrorTask
      }
    `
  );
}

async function addDoNothingTask() {
  await baseApi.mutate(
    gql`
      mutation {
        addDoNothingTask
      }
    `
  );
}

async function getServerEvents({ name, fromDatetime, offset, limit }) {
  const { serverEvents } = await baseApi.query(
    gql`
      query {
        serverEvents
          ${fieldsToString({
            name,
            fromDatetime,
            offset,
            limit
          })}
        {
          name
          data
          createdAt
        }
      }
    `
  );
  return serverEvents;
}

export default {
  isRunning,
  triggerEveryMinuteTask,
  addThrowErrorTask,
  addDoNothingTask,
  getServerEvents
};
