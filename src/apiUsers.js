import gql from 'graphql-tag';
import assert from 'assert';

import { toString, fieldsToString } from './helpers';
import baseApi from './baseApi';

async function createUser({ email, password, firstName, lastName }) {
  const { createUser: { id } } = await baseApi.mutate(
    gql`
      mutation {
        createUser(
          email: ${toString(email)}
          password: ${toString(password)}
          firstName: ${toString(firstName)}
          lastName: ${toString(lastName)}
        ){
          id
        }
      }
    `
  );

  return { id };
}

async function login({ email, password }) {
  const { login: { user, token } } = await baseApi.mutate(
    gql`
      mutation {
        login(
          email: ${toString(email)}
          password: ${toString(password)}
        ){
          token
          user {
            id
            email
            type
            firstName
            lastName
            isVerified
            profileImage {
              id
              path
            }
          }
        }
      }
    `
  );

  return { user, token };
}

async function loginWithFacebook({ fbAccessToken }) {
  const { loginWithFacebook: { user, token } } = await baseApi.mutate(
    gql`
      mutation {
        loginWithFacebook(
          fbAccessToken: ${toString(fbAccessToken)}
        ){
          token
          user {
            id
            email
            firstName
            lastName
          }
        }
      }
    `
  );

  return { user, token };
}

async function currentUser() {
  const { currentUser: user } = await baseApi.query(
    gql`
      query {
        currentUser {
          id
          email
          type
          firstName
          lastName
          isVerified
          profileImage {
            id
            path
          }
        }
      }
    `
  );

  return user;
}

async function userExist({ email }) {
  const { userExist: userExist } = await baseApi.query(
    gql`
      query {
        userExist(email: ${toString(email)})
      }
    `
  );

  return { userExist };
}

async function userProfile({ userId }) {
  const { users: [user] } = await baseApi.query(
    gql`
      query {
        users(ids: [${toString(userId)}]) {
          id
          email
          firstName
          lastName
          bio
          location
          hometown
          dateOfBirth
          profileImage {
            id
            path
          }
        }
      }
    `
  );

  return user;
}

async function currentUserIsOnboarded() {
  const { currentUser: { isOnboarded } } = await baseApi.query(
    gql`
      query {
        currentUser {
          id
          isOnboarded
        }
      }
    `
  );

  return isOnboarded;
}

async function currentUserVerificationHasSkippedImages() {
  const {
    currentUser: { verification: { hasSkippedImages } }
  } = await baseApi.query(
    gql`
      query {
        currentUser {
          id
          verification {
            hasSkippedImages
          }
        }
      }
    `
  );

  return hasSkippedImages;
}

async function currentUserVerificationImages() {
  const { currentUser: { verification: { images } } } = await baseApi.query(
    gql`
      query {
        currentUser {
          id
          verification {
            images {
              id
              path
            }
          }
        }
      }
    `
  );

  return images;
}

async function updateUserIsOnboarded({ userId, isOnboarded }) {
  await baseApi.mutate(
    gql`
      mutation {
        updateUser(
          userId: ${toString(userId)}
          isOnboarded: ${toString(isOnboarded)}
        ) {
          id
          isOnboarded
        }
      }
    `
  );
}

async function updateUserVerificationImages({
  userId,
  hasSkippedImages,
  imageIds
}) {
  await baseApi.mutate(
    gql`
      mutation {
        updateUserVerificationImages
          ${fieldsToString({
            userId,
            hasSkippedImages,
            imageIds
          })}
        {
          id
          verification {
            hasSkippedImages
            images {
              id
              path
            }
          }
        }
      }
    `
  );
}

async function updateUserProfile({
  userId,
  email,
  firstName,
  lastName,
  dateOfBirth,
  bio,
  profileImageId
}) {
  await baseApi.mutate(
    gql`
      mutation {
        updateUser
          ${fieldsToString({
            userId,
            email,
            firstName,
            lastName,
            dateOfBirth,
            bio,
            profileImageId
          })}
        {
          id
          email
          firstName
          lastName
          dateOfBirth
          profileImage {
            id
            path
          }
          bio
        }
      }
    `
  );
}

const UserType = {
  USER: 'USER',
  ADMIN: 'ADMIN',
  SUPERADMIN: 'SUPERADMIN'
};

export default {
  UserType,
  login,
  loginWithFacebook,
  currentUser,
  createUser,
  userProfile,
  userExist,
  currentUserIsOnboarded,
  currentUserVerificationHasSkippedImages,
  currentUserVerificationImages,
  updateUserProfile,
  updateUserVerificationImages,
  updateUserIsOnboarded
};
