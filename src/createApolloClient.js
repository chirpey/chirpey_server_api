import { ApolloClient } from 'apollo-client';
import fetch from 'fetch-everywhere';
import { InMemoryCache } from 'apollo-cache-inmemory';

import createHttpLink from './createHttpLink';

function createApolloClient({
  getUserToken,
  serverHost,
  serverPort,
  graphqlPath,
  customFetch = fetch
}) {
  const link = createHttpLink({
    serverHost,
    serverPort,
    graphqlPath,
    getUserToken,
    customFetch
  });
  const cache = new InMemoryCache();
  const client = new ApolloClient({ link, cache });
  return client;
}

export default createApolloClient;
