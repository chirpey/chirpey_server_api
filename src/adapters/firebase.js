import _firebase from 'firebase';
import _ from 'lodash';
import Rx from 'rxjs';

class Firebase {
  init({ apiKey, authDomain, databaseUrl, storageBucket }) {
    // Only initialise once
    if (this.app) {
      return;
    }

    this.app = _firebase.initializeApp({
      apiKey,
      authDomain,
      databaseURL: databaseUrl,
      storageBucket
    });

    this._observables = [];
  }

  async signIn({ sessionToken }) {
    await _firebase.auth().signInWithCustomToken(sessionToken);
    this._resetObservables();
    await this._onConnect.first().toPromise();
    return {
      onConnect: this._onConnect,
      onDisconnect: this._onDisconnect
    };
  }

  async signOut({}) {
    await _firebase.auth().signOut();
  }

  async getRef({
    refPath,
    orderByChildPath = null,
    limitFirst = null,
    limitLast = null,
    startAt = null,
    endAt = null
  }) {
    const ref = _firebase.database().ref(refPath);
    const query = this._sortLimit(ref, {
      orderByChildPath,
      limitFirst,
      limitLast,
      startAt,
      endAt
    });
    const value = await this._getQueryValue(query);
    log(`Retrieved value of ${refPath}`, value);
    return value;
  }

  async subscribeToRef({
    refPath,
    orderByChildPath = null,
    limitFirst = null,
    limitLast = null,
    startAt = null,
    endAt = null
  }) {
    const onUpdate = new Rx.Subject();
    const ref = _firebase.database().ref(refPath);

    ref.on('value', async snapshot => {
      const query = this._sortLimit(snapshot.ref, {
        orderByChildPath,
        limitFirst,
        limitLast,
        startAt,
        endAt
      });
      const value = await this._getQueryValue(query);
      log(`Notified of change in value of ${refPath}`, value);
      onUpdate.next(value);
    });

    this._observables.push(onUpdate);
    return onUpdate;
  }

  async setRef({ refPath, value }) {
    await _firebase
      .database()
      .ref(refPath)
      .set(value);
    log(`Set value of ${refPath}`, value);
  }

  async updateRef({ refPath, value }) {
    await _firebase
      .database()
      .ref(refPath)
      .update(value);
    log(`Updated value of ${refPath}`, value);
  }

  _sortLimit(ref, { orderByChildPath, limitFirst, limitLast, startAt, endAt }) {
    let newRef = ref;
    if (orderByChildPath) {
      newRef = newRef.orderByChild(orderByChildPath);
    }
    if (startAt) {
      newRef = newRef.startAt(startAt);
    }
    if (endAt) {
      newRef = newRef.endAt(endAt);
    }
    if (limitFirst) {
      newRef = newRef.limitToFirst(limitFirst);
    }
    if (limitLast) {
      newRef = newRef.limitToLast(limitLast);
    }
    return newRef;
  }

  async _resetObservables() {
    // Complete previous connections' observables
    [this._onConnect, this._onDisconnect, ...this._observables].forEach(x => {
      if (x) {
        x.complete();
      }
    });

    this._onConnect = new Rx.Subject();
    this._onDisconnect = new Rx.Subject();
    this._observables = [];

    // Ensure that observers get attached before observable emits
    setTimeout(() => {
      const connectedRef = _firebase.database().ref('.info/connected');
      connectedRef.on('value', snap => {
        if (snap.val()) {
          log('Connected to Firebase Database.');
          this._onConnect.next();
        } else {
          log('Disconnected from Firebase Database.');
          this._onDisconnect.next();
        }
      });
    });
  }

  async _getQueryValue(query) {
    return await new Promise((resolve, reject) => {
      query.once(
        'value',
        snapshot => {
          let value = snapshot.val();
          if (value === null) {
            value = {}; // Standardise all `nulls` to empty maps
          }
          resolve(value);
        },
        error => {
          console.error(error);
          reject(error);
        }
      );
    });
  }
}

function log(...message) {
  console.log('[adapters/firebase]', ...message);
}

function arrayToArrayMap(array) {
  return _.mapValues(_.keyBy(array), () => true);
}

function addToArrayMap(map, value) {
  map[value] = true;
  return map;
}

function arrayMapToArray(map) {
  return Object.keys(map);
}

const firebase = new Firebase();
export default firebase;
export { Firebase, arrayToArrayMap, addToArrayMap, arrayMapToArray };
