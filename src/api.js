import apiDev from './apiDev';
import apiDialog from './apiDialog';
import apiCommon from './apiCommon';
import apiUsers from './apiUsers';
import apiFlocks from './apiFlocks';
import baseApi from './baseApi';
import firebase from './adapters/firebase';
import { version } from '../package.json';

export default {
  getVersion: () => version,
  init: ({
    serverHost,
    serverPort,
    firebaseApiKey,
    firebaseAuthDomain,
    firebaseDatabaseUrl,
    firebaseStorageBucket,
    graphqlPath,
    customFetch
  }) => {
    baseApi.init({
      serverHost,
      serverPort,
      graphqlPath,
      customFetch
    });
    firebase.init({
      apiKey: firebaseApiKey,
      authDomain: firebaseAuthDomain,
      databaseUrl: firebaseDatabaseUrl,
      storageBucket: firebaseStorageBucket
    });
  },
  setUserToken: (...x) => baseApi.setUserToken(...x),
  getUserToken: (...x) => baseApi.getUserToken(...x),
  getUrl: (...x) => baseApi.getUrl(...x),
  ...apiCommon,
  ...apiDev,
  ...apiDialog,
  ...apiUsers,
  ...apiFlocks
};
