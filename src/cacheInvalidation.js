import _ from 'lodash';
import escapeRegexString from 'escape-regex-string';

const ROOT_QUERY = 'ROOT_QUERY';

export function invalidateObject(cacheData, objectName, objectId) {
  // Iterate through each item in cache data and delete all matches to object & its properties
  for (const itemKey in cacheData) {
    // Object name concat with its id must match
    if (
      !itemKey.match(
        new RegExp(
          `^\\$?${escapeRegexString(objectName)}\\:${escapeRegexString(
            objectId
          )}`
        )
      )
    ) {
      continue;
    }

    // Matching item, delete it
    _.unset(cacheData, itemKey);

    log(`Invalidated '${itemKey}' in cache`);
  }
}

export function invalidateQuery(cacheData, queryName, queryProps = {}) {
  // Iterate through each item in cache data and delete any matches to query
  for (const itemKey in cacheData[ROOT_QUERY]) {
    // Query name must match
    if (!itemKey.match(new RegExp(`^${escapeRegexString(queryName)}\\(?`))) {
      continue;
    }

    // All query props must match
    let itemQueryProps = {};
    const propsMatch = itemKey.match(/\(.*(\{.+\}).*\)/);
    if (propsMatch) {
      itemQueryProps = JSON.parse(propsMatch[1]);
    }

    for (const propKey in queryProps) {
      if (itemQueryProps[propKey] != queryProps[propKey]) {
        continue;
      }
    }

    // Matching item, delete it
    _.unset(cacheData[ROOT_QUERY], itemKey);

    log(`Invalidated Query '${itemKey}' in cache`);
  }
}

function log(...x) {
  console.log('[api/invalidation]', ...x);
}
