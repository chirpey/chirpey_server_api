import assert from 'assert';
import path from 'path';
import FormData from 'form-data';
import _ from 'lodash';

import baseApi from './baseApi';
import {
  JavascriptEnvironmentType,
  getJavascriptEnvironmentType,
  getFileNameFromUri
} from './helpers';

async function uploadImages({ images }) {
  const formData = new FormData();
  for (const image of images) {
    assert(image.uri);
    const imageData = getImageData({
      imageUri: image.uri,
      imageType: image.type
    });
    formData.append('images', imageData);
  }

  const result = await baseApi.fetch('/upload', {
    method: 'POST',
    body: formData
  });

  return result;
}

function getImageData({ imageUri, imageType }) {
  const envType = getJavascriptEnvironmentType();
  try {
    // `!envType` prevents code analysis that would pull up the import 'fs' in other JS environments
    const FS = !envType || 'fs';
    const fs = require(FS);
    const imageData = fs.createReadStream(imageUri);
    return imageData;
  } catch (error) {
    if (envType == JavascriptEnvironmentType.REACT_NATIVE) {
      const type = imageType || inferTypeFromImageUri(imageUri) || undefined;
      const name = getFileNameFromUri(imageUri);
      const imageData = {
        uri: imageUri,
        type,
        name
      };
      return imageData;
    } else {
      throw new Error('Environment type unsupported!');
    }
  }
}

function inferTypeFromImageUri(uri) {
  const ext = path.extname(uri);
  if (['.jpg', 'jpeg'].includes(ext)) {
    return 'image/jpeg';
  } else if (['.png'].includes(ext)) {
    return 'image/png';
  }
  // File extension not recognized
  return null;
}

export default {
  uploadImages
};
