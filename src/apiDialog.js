import gql from 'graphql-tag';
import _ from 'lodash';
import assert from 'assert';
import moment from 'moment';

import baseApi from './baseApi';
import dialogManager from './utils/dialogManager';
import { toString } from './helpers';

async function sendMessageInDialog({ dialogUserId, dialogId, text }) {
  assert(dialogId);
  assert(dialogUserId);
  assert(text);

  let message = await dialogManager.sendMessageInDialog({
    dialogId,
    dialogUserId,
    text
  });

  [message] = await populateUsersInMessages([message]);

  return message;
}

async function sendMessageInDialogViaServer({ dialogId, userId, text }) {
  assert(userId);
  assert(dialogId);
  assert(text);

  let { sendMessageInDialog: message } = await baseApi.mutate(
    gql`
      mutation {
        sendMessageInDialog(
          userId: ${toString(userId)}
          dialogId: ${toString(dialogId)}
          text: ${toString(text)}
        ) {
          id
          text
          senderId
          sentAt
        }
      }
    `
  );

  [message] = await populateUsersInMessages([message]);
  return message;
}

async function getMessagesInDialog({ dialogId, limit = 100, offset = 0 }) {
  assert(dialogId);

  let messages = await dialogManager.getMessagesInDialog({
    dialogId,
    limit,
    offset
  });

  messages = await populateUsersInMessages(messages);
  return messages;
}

async function createDialogSession({ userId }) {
  assert(userId);

  // Generate dialog session token on server
  const {
    createDialogSession: { fbSessionToken, dialogUserId }
  } = await baseApi.mutate(
    gql`
      mutation {
        createDialogSession(
          userId: ${toString(userId)}
        ){
          fbSessionToken
          dialogUserId
        }
      }
    `
  );

  const { onDisconnect, onConnect } = await dialogManager.createSession({
    sessionToken: fbSessionToken
  });

  const { onMessage } = await dialogManager.getDialogUserEvents({
    dialogUserId
  });

  return {
    dialogUserId,
    onDisconnect,
    onConnect,
    onMessage
  };
}

async function destroyDialogSession() {
  await dialogManager.destroySession({});
}

async function populateUsersInMessages(messages) {
  // Get users for each message
  const dialogUserIds = _.uniq(messages.map(x => x.senderId));
  const { usersByDialogUserIds: users } = await baseApi.query(
    gql`
      query {
        usersByDialogUserIds(dialogUserIds: ${toString(dialogUserIds)}) {
          id
          firstName
          lastName
          profileImage {
            id
            path
          }
          dialogUserId
        }
      }
    `
  );
  const usersByDialogUserIds = _.keyBy(users, 'dialogUserId');
  messages.forEach(x => {
    x.sender = usersByDialogUserIds[x.senderId];
  });
  return messages;
}

export default {
  sendMessageInDialog,
  sendMessageInDialogViaServer,
  getMessagesInDialog,
  createDialogSession,
  destroyDialogSession
};
