import assert from 'assert';
import fetch from 'fetch-everywhere';
import _ from 'lodash';

import createApolloClient from './createApolloClient';

class BaseApi {
  init({
    serverHost,
    serverPort,
    graphqlPath = '/graphql',
    customFetch = fetch
  }) {
    this.customFetch = customFetch;
    this.serverHost = serverHost;
    this.serverPort = serverPort;
    this.token = null;
    this.isInited = true;
    this.apolloClient = createApolloClient({
      getUserToken: () => this.token,
      serverHost,
      serverPort,
      graphqlPath,
      customFetch
    });
  }

  setUserToken(token) {
    // Has token changed?
    if (token == this.token) {
      return;
    }
    this.token = token;
  }

  getUserToken() {
    return this.token;
  }

  async query(graphqlQuery, { useCache } = {}) {
    assert(this.isInited);

    const sourceBody = _.get(graphqlQuery, 'loc.source.body');
    log(
      `Sending GraphQL query ${useCache ? '(cache-first)' : ''}`,
      graphqlQuery,
      sourceBody ? ` with source body: \n\`\`\`${sourceBody}\n\`\`\`` : ''
    );

    try {
      const { data } = await this.apolloClient.query({
        query: graphqlQuery,
        fetchPolicy: useCache ? 'cache-first' : 'network-only'
      });

      log('Received result of GraphQL query', getObjectPrettyPrint(data));

      return data;
    } catch (gqlError) {
      log('Error occured during GraphQL query', gqlError);
      const error = processGqlError(gqlError);
      throw error;
    }
  }

  async mutate(graphqlMutation) {
    assert(this.isInited);

    const sourceBody = _.get(graphqlMutation, 'loc.source.body');
    log(
      'Sending GraphQL mutation',
      graphqlMutation,
      sourceBody ? ` with source body: \n\`\`\`${sourceBody}\n\`\`\`` : ''
    );

    try {
      const { data } = await this.apolloClient.mutate({
        mutation: graphqlMutation
      });

      log('Received result of GraphQL mutation', getObjectPrettyPrint(data));
      return data;
    } catch (gqlError) {
      log('Error occured during GraphQL mutation', gqlError);
      const error = processGqlError(gqlError);
      throw error;
    }
  }

  async fetch(path, { method, body }) {
    assert(this.isInited);

    const host = this.serverHost;
    const port = this.serverPort;
    const url = `http://${host}:${port}${path}`;

    log('Sending', method, url, 'with body', body, '...');

    const headers = {};

    try {
      const response = await this.customFetch(url, { method, body, headers });
      // All server's responses are in JSON, unless error
      const resultJson = await response.text();
      let result = null;
      try {
        result = JSON.parse(resultJson);
      } catch (error) {
        // Figure error from raw response text
        const error = getErrorFromResponse(resultJson);
        throw error;
      }
      log('Result of HTTP request:', getObjectPrettyPrint(result));

      // All server responses come with `success` flag
      if (!result.success) {
        throw new Error(result.error.message);
      }
      return result;
    } catch (error) {
      log('Error occured during HTTP request', error);
      throw error;
    }
  }

  getUrl(filePath) {
    assert(this.isInited);
    if (!filePath) {
      return null;
    }
    return `http://${this.serverHost}:${this.serverPort}${filePath}`;
  }
}

function processGqlError(gqlError) {
  if (
    gqlError.networkError &&
    gqlError.networkError.result &&
    gqlError.networkError.result.errors &&
    gqlError.networkError.result.errors.length > 0
  ) {
    // Return GraphQL interface network errors as single user-friendly message
    gqlError.message = gqlError.networkError.result.errors[0].message;
  } else if (gqlError.graphQLErrors && gqlError.graphQLErrors.length > 0) {
    // Return GraphQL resolver errors clearly as single user-friendly message
    gqlError.message = gqlError.graphQLErrors[0].message;
    // Remove mention of "GraphQL" to forward error message to client
    gqlError.message = gqlError.message.replace(/^GraphQL\serror:\s/, '');
  } else {
    // Default error message
    gqlError.message = 'Error connecting to server.';
  }

  return gqlError;
}

function getErrorFromResponse(responseText) {
  if (
    new RegExp('<title>413 Request Entity Too Large</title>').exec(responseText)
  ) {
    return new Error('Your file is too large to upload.');
  }

  log('Server error:', responseText);
  return new Error('Sorry, a server error occured...');
}

function log(...x) {
  console.log('[api/baseApi]', ...x);
}

function getObjectPrettyPrint(object) {
  return `\n\`\`\`\n${JSON.stringify(object, null, 2)}\n\`\`\``;
}

const baseApi = new BaseApi();
export default baseApi;
export { BaseApi };
