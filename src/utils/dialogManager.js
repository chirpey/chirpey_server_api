import Rx from 'rxjs';
import assert from 'assert';
import _ from 'lodash';
import moment from 'moment';

import def from './dialogDef';
import firebase, {
  addToArrayMap,
  arrayToArrayMap,
  arrayMapToArray
} from '../adapters/firebase';

class DialogManager {
  async createSession({ sessionToken }) {
    assert(sessionToken);

    const { onConnect, onDisconnect } = await firebase.signIn({ sessionToken });

    return {
      onConnect,
      onDisconnect
    };
  }

  async destroySession({}) {
    await firebase.signOut({});
  }

  async sendMessageInDialog({ dialogUserId, dialogId, text }) {
    assert(dialogUserId);
    assert(dialogId);
    assert(text);

    let message = def.makeDialogMessage({
      senderId: dialogUserId,
      text
    });

    const refPath = def.RefPath.DIALOG_MESSAGE({
      dialogId,
      messageId: message.id
    });

    await firebase.updateRef({
      refPath,
      value: message
    });

    message = def.processTimestampsOfMessage(message);

    return message;
  }

  async getMessagesInDialog({ dialogId, limit = 100, offset = 0 }) {
    assert(dialogId);
    assert(_.isNumber(limit) && limit > 0);
    assert(_.isNumber(offset) && offset >= 0);

    const refPath = def.RefPath.DIALOG_MESSAGES({ dialogId });
    let messages = await firebase.getRef({
      refPath,
      orderByChildPath: 'sentAt',
      limitLast: limit,
      endAt: offset
    });

    messages = Object.values(messages);
    messages = _.sortBy(messages, x => moment(x.sentAt).valueOf()); // Sort in chronological order
    messages = messages.map(def.processTimestampsOfMessage);

    return messages;
  }

  async getDialogUserEvents({ dialogUserId }) {
    assert(dialogUserId);

    const refPath = def.RefPath.DIALOG_USER_DIALOG_IDS({ dialogUserId });
    const onDialogIdsChange = await firebase.subscribeToRef({ refPath });
    const onMessage = new Rx.Subject();
    let onDialogChangeSubs = [];
    onDialogIdsChange.subscribe({
      next: async dialogIds => {
        // Unsubscribe from previous dialogs
        onDialogChangeSubs.forEach(x => x.unsubscribe());
        const dialogsEvents = await Promise.all(
          Object.keys(dialogIds).map(x => {
            return this._getDialogEvents({ dialogId: x });
          })
        );
        onDialogChangeSubs = dialogsEvents.map(
          ({ dialogId, onDialogMessagesUpdate }) => {
            return onDialogMessagesUpdate.subscribe({
              next: messages => {
                onMessage.next({
                  dialogId
                });
              }
            });
          }
        );
      }
    });

    return {
      onMessage
    };
  }

  async _getDialogEvents({ dialogId }) {
    const refPath = def.RefPath.DIALOG_MESSAGES({ dialogId });
    const onDialogMessagesUpdate = await firebase.subscribeToRef({ refPath });
    return {
      dialogId,
      onDialogMessagesUpdate
    };
  }
}

const dialogManager = new DialogManager();
export default dialogManager;
