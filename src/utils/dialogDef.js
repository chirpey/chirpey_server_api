import uuid from 'uuid';
import moment from 'moment';
import assert from 'assert';
import firebase from 'firebase';

const KEY_DIALOGS = 'dialogs';
const KEY_DIALOG_USERS = 'dialogUsers';

function makeDialogUser({ id }) {
  assert(id);

  const object = makeObject({ id });
  return {
    ...object,
    dialogIds: {}
  };
}

function makeDialog({ id }) {
  assert(id);

  const object = makeObject({ id });
  return {
    ...object,
    userIds: {},
    messages: {}
  };
}

function makeDialogMessage({ text, senderId }) {
  assert(text);
  assert(senderId);

  const object = makeObject({});

  return {
    ...object,
    sentAt: firebase.database.ServerValue.TIMESTAMP,
    text,
    senderId
  };
}

function makeObject({ id = uuid.v4() }) {
  return {
    id,
    createdAt: firebase.database.ServerValue.TIMESTAMP,
    deletedAt: null
  };
}

function deleteObject(object) {
  return {
    ...object,
    deletedAt: firebase.database.ServerValue.TIMESTAMP
  };
}

function processTimestampsOfMessage(message) {
  message.sentAt = moment(message.sentAt).toISOString();
  return message;
}

const RefPath = {
  DIALOG({ dialogId }) {
    return `/${KEY_DIALOGS}/${dialogId}`;
  },
  DIALOG_MESSAGE({ dialogId, messageId }) {
    return `${RefPath.DIALOG_MESSAGES({ dialogId })}/${messageId}`;
  },
  DIALOG_MESSAGES({ dialogId }) {
    return `${RefPath.DIALOG({ dialogId })}/messages`;
  },
  DIALOG_USER_IDS({ dialogId }) {
    return `${RefPath.DIALOG({ dialogId })}/userIds`;
  },
  DIALOG_USER({ dialogUserId }) {
    return `/${KEY_DIALOG_USERS}/${dialogUserId}`;
  },
  DIALOG_USER_DIALOG_IDS({ dialogUserId }) {
    return `${RefPath.DIALOG_USER({ dialogUserId })}/dialogIds`;
  }
};

export default {
  makeDialog,
  makeDialogUser,
  makeDialogMessage,
  processTimestampsOfMessage,
  deleteObject,
  RefPath
};
